<footer>
		
			<div class="row">
				<div class="col-sm-6 text-center-mobile">
					<h3 class="white">Practice more and more, dear!</h3>
					<h5 class="light regular light-white">You will surely be good at cooking.</h5>
					<a href="main.jsp" class="btn btn-blue ripple trial-button">Return to Home Page</a>
				</div>
				<div class="col-sm-6 text-center-mobile">
					<h3 class="white">Green Light is always here for you! <span class="open-blink"></span></h3>
					<div class="row opening-hours">
						<div class="col-sm-6 text-center-mobile">
							<h5 class="light-white light">Darling, keep in mind</h5>
							<h3 class="regular white">just try it.</h3>
						</div>
						<div class="col-sm-6 text-center-mobile">
							<h5 class="light-white light">Granny is always here to help you</h5>
							<h3 class="regular white">How about one more recipe?</h3>
						</div>
					</div>
				</div>
			</div>
			<div class="row bottom-footer text-center-f">
				<div class="col-sm-8">
					<p>&copy; 2016 All Rights Reserved.</p>
				</div>
				<div class="col-sm-4 text-right text-center-mobile">
					
				</div>
			</div>
		
	</footer>