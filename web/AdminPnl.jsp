<%-- 
    Document   : Admin
    Created on : 07.03.2016, 14:45:30
    Author     :Natavan
--%>
<%@page errorPage="ErrorPage.jsp"%>
<%@include file="loginCheck.jsp" %> 
<%@page import="com.recipes.Db.CategoryDb"%>
<%@page import="java.util.ArrayList"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@page errorPage="ErrorPage.jsp"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7" crossorigin="anonymous">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js" integrity="sha384-0mSbJDEHialfmuBBQP6A4Qrprq5OVfW37PRR3j5ELqxss1yVqOtnepnHVP9aJ7xS" crossorigin="anonymous"></script>

        <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7" crossorigin="anonymous">
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js" integrity="sha384-0mSbJDEHialfmuBBQP6A4Qrprq5OVfW37PRR3j5ELqxss1yVqOtnepnHVP9aJ7xS" crossorigin="anonymous"></script>
        <title>Admin Panel</title>
    </head>

    <body>

        <style>           
            a:hover{
                text-decoration: none;
            }
            ul{
                list-style: none;
            }
            ul li{
                float: left;
                margin:10px 10px 0px;
            }
            .dropdown-header{
                width:100%;
            }
            .table {
                display: table;
                border-collapse: separate;
                border-spacing: 40px 50px;
            }
            .tableRow {
                display: table-row;
            }
            .tableCell {
                display: table-cell;
            }
            .box1 {
                border: 10px solid rgba(238, 238, 238, 0.5);
                width: 250px;
                height: 200px;
            }
        </style>
        <% request.setAttribute("allCategories", CategoryDb.getAllCategories());
            HttpSession session1 = request.getSession();
            ArrayList<String> images = (ArrayList) session1.getAttribute("links");
            if (images != null) {
                images.clear();
            } else {
                //do nothig
            }

        %>

        <c:import url="/WEB-INF/tags/adminHeader.tag"/>
        
        <div class="ch-container">
            <div class="row">

                <!-- left menu -->
                <div class="col-sm-2 col-lg-2">
                    <div class="sidebar-nav">
                        <div class="nav-canvas">
                            <div class="nav-sm nav nav-stacked">

                            </div>
                            <ul class="collapse navbar-collapse nav nav-pills nav-stacked main-menu">
                                <li class="nav-header"></li>
                                <li class="active"><a class="ajax-link" href="AdminPnl.jsp"><i class="glyphicon glyphicon-home"></i><span> Dashboard</span></a>
                                </li>
                                <li><a class="ajax-link" href="#"><i class="glyphicon glyphicon-list-alt"></i><span> Charts</span></a>
                                </li>
                                <li><a class="ajax-link" href="AdminPnl_Table.jsp"><i
                                            class="glyphicon glyphicon-align-justify"></i><span> Tables</span></a></li>                        
                                <li><a class="ajax-link" href="#"><i class="glyphicon glyphicon-calendar"></i><span> Calendar</span></a>
                                </li>
                            </ul>                    
                        </div>
                    </div>
                </div>

                <noscript>
                <div class="alert alert-block col-md-12">
                    <h4 class="alert-heading">Warning!</h4>

                    <p>You need to have <a href="http://en.wikipedia.org/wiki/JavaScript" target="_blank">JavaScript</a>
                        enabled to use this site.</p>
                </div>
                </noscript>

                <div id="content" class="col-lg-10 col-sm-10">
                    <div>
                        <ul class="breadcrumb">
                            <li>
                                <a href="#">Home</a>
                            </li>
                        </ul>
                    </div>

                    <div class="row">
                        <div class="box col-md-12">
                            <div class="box-inner">
                                <div class="box-header well" data-original-title="">
                                    <h2><i class="glyphicon glyphicon-apple"></i> Add Recipe</h2>        
                                </div>
                                <form action="AddRecipeServlet" method="post" >
                                    <div class="box-content col-md-6">

                                        <div class="form-group">
                                            <label class="control-label" for="recipeName">Recipe Name</label>
                                            <input type="text" id="recipeName" class="form-control" name="recipe_name"/>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label" for="desc">Recipe Description</label>
                                            <input type="text" id="recipeDesc" class="form-control" name="recipe_desc"/>
                                        </div>

                                        <div class="form-group dropdown">
                                            <label class="control-label" for="category">Category</label>
                                            <select class="form-control" data-style="btn btn-success"  name="category"  id="category"  >
                                                 <c:forEach items="${allCategories}" var="cat">
                                                <option>${cat.category_name} </option>
                                                  </c:forEach>
                                                <option>  Lunch </option>
                                                <option> Beverages</option>
                                                <option> Appetizers</option>
                                                <option> Soups</option>
                                                <option> Salads</option>
                                                <option> Main dishes: Beef</option>
                                                <option>Main dishes: Poultry</option>
                                                <option> Main dishes: Pork</option>
                                                <option> Main dishes: Seafood</option>
                                                <option> Main dishes: Vegetarian</option>
                                                <option> Side dishes: Vegetables</option>
                                                <option> Side dishes: Other</option>
                                                <option> Desserts</option>
                                                <option> Canning / Freezing</option>
                                                <option> Breads</option>
                                                <option> Holidays</option>
                                                <option> Entertaining</option>
                                            </select>
                                            <a data-toggle="model" class="category-add" href="">Add new category</a>
                                        </div>

                                        <div class="form-group">
                                            <label class="control-label" for="services">Services</label>
                                            <input type='number' id="services" class="form-control"  name="services"/>
                                        </div> 
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group" id="ingr1">

                                                    <b>Ingredient 1</b> <input type='text' id="ingredients" class="form-control"  name="ingredients" style="width: 200px"/>
                                                </div>
                                            </div>
                                            <div class="col-md-3">
                                                <div class="form-group" id="amount1">
                                                    <b>Amount</b><input type='number' id="instruction" class="form-control"  name="amount" style="width: 70px"/>
                                                </div>
                                            </div>
                                            <div class="col-md-3">
                                                <div class="form-group" id="uom1">
                                                    <b> Unit of Measure </b><select class="form-control" data-style="btn btn-success"  name="uom"  id="uom" style="width: 100px" >

                                                        <option>kg</option>             
                                                        <option>g</option>
                                                        <option>litre</option>
                                                        <option>cup</option>
                                                        <option>tea spoon</option>
                                                        <option>table spoon</option>
                                                        <option>package</option>
                                                        <option>ounce</option>


                                                    </select> 
                                                </div> 
                                            </div>&nbsp &nbsp <i class="glyphicon glyphicon-plus-sign" style="font-size: 30px" onclick="addInputForIngredient('ingr1', 'amount1', 'uom1')"></i> 
                                        </div>  
                                        <br/>
                                        <div class="row">
                                            <div class="col-md-3">
                                                <div class="form-group" id="step1">

                                                    <b>Step</b>  <input type='number' id="step" class="form-control"  name="step" value="1" style="width: 50px"/>
                                                </div>
                                            </div>
                                          
                                            <div class="col-md-9">
                                                <div class="form-group" id="direction1">
                                                    <b>Direction </b> <textarea id="instruction" class="form-control"  name="direction" style="width: 350px"></textarea> 
                                                </div>
                                            </div>
                                            &nbsp &nbsp <i class="glyphicon glyphicon-plus-sign" style="font-size: 30px" onclick="addInputForDirection('step1', 'direction1')"></i> 
                                        </div>
                                        </br>




                                        <button type="submit" class="btn btn-success">Add</button>  
                                    </div>
                                </form>
                            </div>


                            <div class="box-content col-md-6">
                                <div class="form-group">
                                    <div class="table">
                                        <div class="tableRow">
                                            <div class="tableCell">
                                                <i class="glyphicon glyphicon-camera"></i>
                                                <input type="file" id="file">
                                            </div>
                                            <div class="tableCell" id="fileInfo">
                                            </div>
                                        </div>
                                        <div class="tableCell box1">
                                            <h2>Image</h2>
                                            <img src="" id="uploadedImage" style="width: 170px; height: 100px; background-color:#efeeee; border: 0px; ">
                                            <br>
                                            <button id="uploadImage" class="btn btn-primary">Upload</button>
                                        </div>

                                    </div>
                                </div>



                            </div>       


                        </div>
                    </div>

                </div>
            </div>
            <div class="modal fade" id="addCategory" aria-hidden="true">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal">×</button>
                            <h3>Add</h3>
                        </div>
                        <div class="modal-body">
                            <form action="AddCategoryServlet" method="post">
                                <div class="form-group">
                                    <label class="control-label" for="recipeName">Category Name</label>
                                    <input type="text" id="categoryName" class="form-control" name="category_name" value=""/>
                                </div>
                                <div class="modal-footer">
                                    <input class="btn btn-primary" type="submit" value="Save Changes"/>
                                    <a href="#" class="btn btn-default" data-dismiss="modal">Cancel</a>
                            </form>
                        </div>

                    </div>
                </div>
            </div>
        </div> 
          

    </div>
    <script src="Admin.js"></script>
    <script src="uploadfile.js"></script>
    <script src="AddInputElement.js"></script>
</body>
</html>
