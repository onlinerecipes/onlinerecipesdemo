<%-- 
    Document   : ErrorPage
    Created on : Mar 30, 2016, 11:47:58 PM
    Author     : Sariya
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7" crossorigin="anonymous">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js" integrity="sha384-0mSbJDEHialfmuBBQP6A4Qrprq5OVfW37PRR3j5ELqxss1yVqOtnepnHVP9aJ7xS" crossorigin="anonymous"></script>

        <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7" crossorigin="anonymous">
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js" integrity="sha384-0mSbJDEHialfmuBBQP6A4Qrprq5OVfW37PRR3j5ELqxss1yVqOtnepnHVP9aJ7xS" crossorigin="anonymous"></script>
        <title>Admin Panel</title>
    </head>

    <body>

        <style>
            #logo{
                margin-top: -15px;
            }
            a:hover{
                text-decoration: none;
            }
            .search-query{
                margin-right: 30px;
            }
            ul{
                list-style: none;
            }
            ul li{
                float: left;
                margin:10px 10px 0px;
            }
            .navbar-inverse{           
                margin-bottom:0;
                margin-top:22%;
            }
            .dropdown-header{
                width:100%;
            }
            .table {
                display: table;
                border-collapse: separate;
                border-spacing: 40px 50px;
            }
            .tableRow {
                display: table-row;
            }
            .tableCell {
                display: table-cell;
            }
            .box1 {
                border: 10px solid rgba(238, 238, 238, 0.5);
                width: 250px;
                height: 200px;
            }
        </style>
        <div class="navbar navbar-default" role="navigation">

           <c:import url="/WEB-INF/tags/AdminHeader.tag"/>
</br>
</br>
</br>
</br>

        <div class="ch-container">
            
            <div id="errorfield" class="row" >
                  <div class="alert alert-danger" role="alert" >
                 <h4>  Oops This recipe already exists, try new one! 
                   <a href="AdminPnl.jsp" class="alert-link">Go back  </a> </h4>
                 </div>
            </div>
            <footer>
                <div class="navbar navbar-inverse">
                    <div class="container">
                        <p class="copyright navbar-text pull-left">© 2016 Dr. Tababat</p>
                    </div>
                </div>
            </footer>

        </div>
        <script src="uploadfile.js"></script>
    </body>
</html>
