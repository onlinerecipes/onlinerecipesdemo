<%-- 
    Document   : IndividualRecipe1
    Created on : Apr 29, 2016, 9:46:10 PM
    Author     : Konul Gurbanli
--%>
<%@page errorPage="UserError.jsp"%>
<%@page import="com.recipes.entities.Image"%>
<%@page import="java.util.ArrayList"%>
<%@page import="com.recipes.entities.Direction"%>
<%@page import="com.recipes.entities.Ingredient"%>
<%@page import="com.recipes.Db.IngredientDb"%>
<%@page import="com.recipes.entities.Recipe"%>
<%@page import="java.util.List"%>
<%@page import="java.util.Iterator"%>
<%@page import="com.recipes.Db.RecipeDb"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Granny’s Tandir</title>
	<meta name="description" content=“Granny’s Tandir has been made for Terms Project” />
	<meta name="keywords" content="html template, css, free, one page, gym, fitness, web design" />
	<meta name="author" content=“Nargis A.” />
	<!-- Favicons (created with http://realfavicongenerator.net/)-->
	<link rel="apple-touch-icon" sizes="57x57" href="img/favicons/apple-touch-icon-57x57.png">
	<link rel="apple-touch-icon" sizes="60x60" href="img/favicons/apple-touch-icon-60x60.png">
	<link rel="icon" type="image/png" href="img/favicons/favicon-32x32.png" sizes="32x32">
	<link rel="icon" type="image/png" href="img/favicons/favicon-16x16.png" sizes="16x16">
	<link rel="manifest" href="img/favicons/manifest.json">
	<link rel="shortcut icon" href="img/favicons/favicon.ico">
	<meta name="msapplication-TileColor" content="#00a8ff">
	<meta name="msapplication-config" content="img/favicons/browserconfig.xml">
	<meta name="theme-color" content="#ffffff">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7" crossorigin="anonymous">
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js" integrity="sha384-0mSbJDEHialfmuBBQP6A4Qrprq5OVfW37PRR3j5ELqxss1yVqOtnepnHVP9aJ7xS" crossorigin="anonymous"></script>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
        <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7" crossorigin="anonymous">
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js" integrity="sha384-0mSbJDEHialfmuBBQP6A4Qrprq5OVfW37PRR3j5ELqxss1yVqOtnepnHVP9aJ7xS" crossorigin="anonymous"></script>        
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js"></script>
        <!-- Normalize -->
	<link rel="stylesheet" type="text/css" href="css/normalize.css">
	<!-- Bootstrap -->
	<link rel="stylesheet" type="text/css" href="css/bootstrap.css">
	<!-- Owl -->
	<link rel="stylesheet" type="text/css" href="css/owl.css">
	<!-- Animate.css -->
	<link rel="stylesheet" type="text/css" href="css/animate.css">
	<!-- Font Awesome -->
	<link rel="stylesheet" type="text/css" href="fonts/font-awesome-4.1.0/css/font-awesome.min.css">
	<!-- Elegant Icons -->
	<link rel="stylesheet" type="text/css" href="fonts/eleganticons/et-icons.css">
	<!-- Main style -->
	<link rel="stylesheet" type="text/css" href="css/grannystandir.css">
<link rel="stylesheet" type="text/css" href="css/2ndsearch.css">
</head>
 <link href='custom.css' rel='stylesheet' type='text/css'>
<link rel="stylesheet" type="text/css" href="css/seacrh.css">

<body>
	<div class="preloader">
		<img src="img/loader.gif" alt="Preloader image">
	</div>
	<nav class="navbar">
		<div class="container">
			<!-- Brand and toggle get grouped for better mobile display -->
			<div class="navbar-header">
				<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
					<span class="sr-only">Toggle navigation</span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
				</button>
                            <a class="navbar-brand" href="main.jsp"><img src="img/lastversionlogo copy.png" data-active-url="img/lastversionlogo copy.png" alt=""></a>
                        </div>
			<!-- Collect the nav links, forms, and other content for toggling -->
			<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
				<ul class="nav navbar-nav navbar-right main-nav">
					<li><a href="main.jsp#home">Home</a></li>
					<li><a href="main.jsp#about">About</a></li>
					<li><a href="main.jsp#recipes">Recipes</a></li>
					<li><a href="main.jsp#contanct">Contact</a></li>
					
				</ul>
			</div>
			<!-- /.navbar-collapse -->
		</div>
		<!-- /.container-fluid -->
	</nav>

    </head>
<div class="cut cut-bottom"></div>
	</section>
	<section id="recipes" class="section gray-bg">
		<div class="container">
			<div class="row title text-center">
				<h2 class="margin-top">Recipes</h2>
				<h4 class="light muted">GOOD CHOICE!</h4>
<h5 class="light muted">This is really tasty:</h5>
			</div>

						<div class="row">
				
				<div class="col-md-4">
					<div class="recipes text-center">
						<div id="pic" class="cover" style="background:url('img/dinner.png'); background-size:cover;">
      <%                                                   
            List l = (List) request.getAttribute("details");
            
           // if(l==null){System.out.println("list is null /individualjsp");}
            request.setAttribute("r",l);
            String imgnm = (String) request.getAttribute("imgnm");
            if(imgnm != null){                
            %>
            <script>
                document.getElementById("pic").style.set("background: url(uploaded-images/${imgnm}); background-size:cover;");
            </script>
            <%}%>    
							<div class="overlay text-center">
								<h3 class="white"></h3>
                                                                <h5 class="light light-white"><b>
                                                                    <%if(l.get(2) != null){%>
                                                                    ${r.get(2)}
                                                                    <%}%>
                                                                    </b></h5>
							</div>
						</div>
						
						<div class="title">
							<h4>${r.get(0)} </h4> <br>
                                                        <h6>
                                                            <%if(l.get(2) != null){%>
                                                                    ${r.get(2)}
                                                                    <%}%><br>
                                                                    </h6>
							<h5 class="muted regular"></h5>
						</div>

					</div>
				</div>
<div class="col-md-4">

    <div class="recipes text-center">
						
         <h4> Category: </h4> ${r.get(1)}
             <br>
             <h4> Rating:</h4> 
             <%if(l.get(4) != null){%>
                        ${r.get(4)}
                        <%}
             else{
                 %>
                 0
                        <%}%>
                        <br>
                        <h4>   Service: </h4> <%if(l.get(3) != null){%> 
                        
                        <dd>${r.get(3)}</dd>
                        <%}%></h3>
    <h4>Ingredients:</h4>
<h5>
<%
                    List ings = (List) request.getAttribute("ingredients");
                    Iterator iter2 = ings.iterator();                    
                    while(iter2.hasNext()){
                    Ingredient i = (Ingredient) iter2.next();
                    request.setAttribute("i",i);%>                     
                    <div class="row">${i.getAmount()} ${i.getUom()} of ${i.getIngredient_name()}</div>
                <% } %> </h5>
</div>					
</div>
<div class="col-md-4">
    <div class="recipes text-center">
	<h3 style="text-align:left">Instructions:</h3>
        
<%
                    List dirs = (List) request.getAttribute("directions");                  
                    Iterator iter3 = dirs.iterator();
                    while(iter3.hasNext()){            
                    Direction d = (Direction) iter3.next();
                    request.setAttribute("d",d);                
                %>
                
                        <dd>${d.getStep()}. ${d.getInstruction()}</dd>
                        <% } %>
                        
    	</div>
		</div>
	</div>
                        
   <script> 
                   function toggleHeart(){
  $("#favorite").toggleClass("glyphicon-heart-empty glyphicon-heart");
  if($("#favorite").hasClass("glyphicon-heart-empty")){document.getElementById("favoriteIn").value="0";}
  else{document.getElementById("favoriteIn").value="1";}
  
}               
                      </script>                      

<div class="col-md-4">
<div class="recipes text-center">
<div class="cover" style="background:url('img/team/comment.jpg'); background-size:cover;">
<div class="overlay text-center">
<h3 class="white">Comment on a recipe.</h3>
<h5 class="light light-white">Make Granny happier</h5>
</div>
</div>
<div class="title">
<form method="POST" action="AddCommentServlet" name="form" id="commentForm">
Name:<input type="text" name="name" required="true" form="commentForm"><br>
 Email:<input type="email" name="email" required="true" form="commentForm"><br>
<input style="display: none" id="test_email" type="text" name="test_email" value="" size="25" form="commentForm">  
            <input style="display: none" id="recipe_id" type="number" name="recipe_id" value="" size="25" form="commentForm">
            <script> document.getElementById("recipe_id").value = "<%=Integer.parseInt(request.getAttribute("recipe_id").toString())%>";
            </script>

  <br>Comment:<br> <br>
  <textarea rows="5" cols="20" name="comment" form="commentForm" id='comment'></textarea><br>
 Love it ?
 <button style="background-color: transparent; border:none;" class="btn btn-default" id="toggle" onclick="toggleHeart()"> 
     <span style="color:#269abc" id="favorite" class="glyphicon glyphicon-heart-empty"></span> 
         </button>
  <input style="display: none" id="favoriteIn" type="number" name="favorite" value="0" size="25">
  <input type="submit" value="post" class="btn btn-default" style="background-color: #269abc; border: none;"> 
</form>
            <script> //toggle love it button
             if(document.getElementById("favoriteIn").value==="1"){
       $("#favorite").toggleClass("glyphicon-heart-empty glyphicon-heart");
    }
            </script>
            
</div>
</div>
</div>
            
                <div id="allCommentsDiv" class="col-sm-4">
        <div class="panel panel-default" style="padding: 10px;" >
                <div class="panel-heading">
                    <div class="panel-title">Comments</div>
                </div>
            <br>
            
        <c:forEach items="${comments}" var="comment">
       <div class="col-md-4"><c:out value="${comment.name}"/></div>
        <br>
        <div class="col-md-4"><c:out value="${comment.comment}"/></div>
        <br>
        <br>
         </c:forEach>                
        </div>
         </div>
                
</section>

<footer style=“position:fixed”>
		<div class="container">
			<div class="row">
				<div class="col-sm-6 text-center-mobile">
					<h3 class="white">Practice more and more, dear!</h3>
					<h5 class="light regular light-white">You will surely be good at cooking.</h5>
					<li><a href="main.jsp" class="btn btn-blue ripple trial-button">Return to Home Page</a></li>
				</div>
				<div class="col-sm-6 text-center-mobile">
					<h3 class="white">Green Light is always here for you! <span class="open-blink"></span></h3>
					<div class="row opening-hours">
						<div class="col-sm-6 text-center-mobile">
							<h5 class="light-white light">Darling, keep in mind</h5>
							<h3 class="regular white">just try it.</h3>
						</div>
						<div class="col-sm-6 text-center-mobile">
							<h5 class="light-white light">Granny is always here to help you</h5>
							<h3 class="regular white">How about one more recipe?</h3>
						</div>
					</div>
				</div>
			</div>
			<div class="row bottom-footer text-center-mobile">
				<div class="col-sm-8">
					<p>&copy; 2016 All Rights Reserved.</p>
				</div>
				<div class="col-sm-4 text-right text-center-mobile">
					
				</div>
			</div>
		</div>
	</footer>
	<!-- Holder for mobile navigation -->
	<div class="mobile-nav">
		<ul>
		</ul>
		<a href="#" class="close-link"><i class="arrow_up"></i></a>
	</div>
	<!-- Scripts -->
	<script src="js/jquery-1.11.1.min.js"></script>
	<script src="js/owl.carousel.min.js"></script>
	<script src="js/bootstrap.min.js"></script>
	<script src="js/wow.min.js"></script>
	<script src="js/typewriter.js"></script>
	<script src="js/jquery.onepagenav.js"></script>
	<script src="js/main.js"></script>
<script src="js/recipetable.js"></script>
</html>