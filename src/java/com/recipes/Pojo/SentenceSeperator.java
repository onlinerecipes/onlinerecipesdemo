/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.recipes.Pojo;

import java.text.BreakIterator;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Aydin Bagiyev
 */
public class SentenceSeperator {
    
    public static List<String> getWords(String text) {
    List<String> words = new ArrayList<String>();
    BreakIterator breakIterator = BreakIterator.getWordInstance();
    breakIterator.setText(text);
    int lastIndex = breakIterator.first();
    while (BreakIterator.DONE != lastIndex) {
    int firstIndex = lastIndex;
    lastIndex = breakIterator.next();
    if (lastIndex != BreakIterator.DONE && Character.isLetterOrDigit(text.charAt(firstIndex))) {
    words.add(text.substring(firstIndex, lastIndex));
            }
       }

    return words;
    }
}
