package com.recipes.Db;

import com.recipes.entities.Category;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;
import javax.persistence.NoResultException;
import javax.persistence.TypedQuery;

/**
 *
 * @author Sariya
 */
public class CategoryDb {
    /*Fetch category from db by id*/

    public static Category getCategoryById(int category_id) {
        EntityManager em = DbUtil.getEmfactory().createEntityManager();
        try {
            Category category = em.find(Category.class, category_id);
            return category;
        } finally {
            em.close();
        }

    }
    /*Fetch category from db by name*/

    public static Category getCategoryByName(String category_name) {
        EntityManager em = DbUtil.getEmfactory().createEntityManager();
        String query = "select d from Category d where d.category_name=:category_name";
        TypedQuery<Category> q = em.createQuery(query, Category.class);
        q.setParameter("category_name", category_name);
        Category category = null;
        try {
            category = q.getSingleResult();
        } catch (NoResultException e) {
            System.out.println(e);
            System.out.println("cannot get category");
        } finally {
            em.close();
        }
        return category;
    }
    /*Fetch all categories*/

    public static List<Category> getAllCategories() {
        EntityManager em = DbUtil.getEmfactory().createEntityManager();
        try {
            return em.createNamedQuery("Category.getAll").getResultList();
        } finally {

            em.close();
        }
    }
    /*insert category*/

    public static void insert(Category category) { //insert new category to db
        EntityManager em = DbUtil.getEmfactory().createEntityManager();
        EntityTransaction trans = em.getTransaction();
        trans.begin();
        try {
            em.persist(category);
            trans.commit();
        } catch (Exception e) {
            System.out.println(e);
            trans.rollback();
        } finally {
            em.close();
        }
    }
    /*Update category*/

    public static void update(Category category) { //update category
        EntityManager em = DbUtil.getEmfactory().createEntityManager();
        EntityTransaction trans = em.getTransaction();
        trans.begin();
        try {
            em.merge(category);
            trans.commit();
        } catch (Exception e) {
            System.out.println(e);
            trans.rollback();
        } finally {
            em.close();
        }
    }
    /*delete category*/

    public static void delete(Category category) { //delete category
        EntityManager em = DbUtil.getEmfactory().createEntityManager();
        EntityTransaction trans = em.getTransaction();
        trans.begin();
        try {
            em.remove(category);
            trans.commit();
        } catch (Exception e) {
            System.out.println(e);
            trans.rollback();
        } finally {
            em.close();
        }
    }
}
