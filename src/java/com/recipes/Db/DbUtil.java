/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.recipes.Db;

import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

/**
 *
 * @author Sariya
 */
public class DbUtil {
    /*Create entity manager*/
    private static final EntityManagerFactory emfactory = Persistence.createEntityManagerFactory("OnlineRecipesPU");
    public static EntityManagerFactory getEmfactory() {
        return emfactory;
    }
    
}
