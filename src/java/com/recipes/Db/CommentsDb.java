package com.recipes.Db;

import com.recipes.entities.Comments;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;
import javax.persistence.NoResultException;
import javax.persistence.TypedQuery;

/**
 *
 * @author Sariya
 */
public class CommentsDb {
    /*Fetch comment from db by id*/

    public static Comments getCommentsById(int comment_id) {
        EntityManager em = DbUtil.getEmfactory().createEntityManager();
        try {
            Comments comment = em.find(Comments.class, comment_id);
            return comment;
        } finally {
            em.close();
        }

    }
    /*Fetch Comment from db by recipe name*/

    
    /*Fetch all comments*/

    public static List<Comments> getAllComments() {
        EntityManager em = DbUtil.getEmfactory().createEntityManager();
        try {
            return em.createNamedQuery("Comments.getAll").getResultList();
        } finally {

            em.close();
        }
    }
    /*insert comment*/

    public static void insert(Comments comment) {
        EntityManager em = DbUtil.getEmfactory().createEntityManager();
        EntityTransaction trans = em.getTransaction();
        trans.begin();
        try {
            em.persist(comment);
            trans.commit();
        } catch (Exception e) {
            System.out.println(e);
            trans.rollback();
        } finally {
            em.close();
        }
    }
    /*Update comment*/

    public static void update(Comments comment) {
        EntityManager em = DbUtil.getEmfactory().createEntityManager();
        EntityTransaction trans = em.getTransaction();
        trans.begin();
        try {
            em.merge(comment);
            trans.commit();
        } catch (Exception e) {
            System.out.println(e);
            trans.rollback();
        } finally {
            em.close();
        }
    }
    /*delete comment*/

    public static void delete(int comment_id) {
        EntityManager em = DbUtil.getEmfactory().createEntityManager();
        EntityTransaction trans = em.getTransaction();
        trans.begin();
        try {
            Comments comment=em.find(Comments.class, comment_id);
            em.remove(comment);
            trans.commit();
        } catch (Exception e) {
            System.out.println(e);
            trans.rollback();
        } finally {
            em.close();
        }
    }
}
