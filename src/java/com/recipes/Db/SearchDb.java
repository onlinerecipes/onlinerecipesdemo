/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.recipes.Db;

import java.util.List;
import javax.persistence.EntityManager;

/**
 *
 * @author Aydin Bagiyev
 */
public class SearchDb {

        public static List searchEachWord(List<String> seperatedSentence){
            String whereClause = "";
            int size = seperatedSentence.size();
        
            
        EntityManager em = DbUtil.getEmfactory().createEntityManager();
        
        for(String x : seperatedSentence){
            
            if(size>1)
            
                whereClause += "a.recipe.recipe_name LIKE '%"+x+"%' OR a.ingredient_name LIKE '%"+x+"%' AND ";
            else
                whereClause +="a.recipe.recipe_name LIKE '%"+x+"%' OR a.ingredient_name LIKE '%"+x+"%'";
            
            size--;
        }
        String sql="SELECT a FROM Ingredient a WHERE "+whereClause;
        List resultList = em.createQuery(sql)
               
                .getResultList();
        return resultList;
    
    }
}

