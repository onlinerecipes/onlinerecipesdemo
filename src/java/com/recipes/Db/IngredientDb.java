/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.recipes.Db;

import com.recipes.entities.Ingredient;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;
import javax.persistence.NoResultException;
import javax.persistence.TypedQuery;

/**
 *
 * @author Sariya
 */
public class IngredientDb {
    /*Fetch ingredient from db by id*/

    public static Ingredient getIngredientById(int ingredient_id) {
        EntityManager em = DbUtil.getEmfactory().createEntityManager();
        try {
            Ingredient ingredient = em.find(Ingredient.class, ingredient_id);
            return ingredient;
        } finally {
            em.close();
        }

    }
    /*Fetch ingredient from db by ingredient name*/

    public static Ingredient getIngredientByName(String ingredient_name) {
        EntityManager em = DbUtil.getEmfactory().createEntityManager();
        String query = "select d from Ingredient d where d.ingredient_name=:ingredient_name";
        TypedQuery<Ingredient> q = em.createQuery(query, Ingredient.class);
        q.setParameter("ingredient_name", ingredient_name);
        Ingredient ingredient = null;
        try {
            ingredient = q.getSingleResult();
        } catch (NoResultException e) {
            System.out.println(e);
             System.out.println("cannot get ingredient");
        } finally {
            em.close();
        }
        return ingredient;
    }
    /*Fetch all ingredients*/

    public static List<Ingredient> getAllIngredients() {
        EntityManager em = DbUtil.getEmfactory().createEntityManager();
        try {
            return em.createNamedQuery("Ingredient.getAll").getResultList();
        } finally {

            em.close();
        }
    }
    /*insert ingredient*/

    public static void insert(Ingredient ingredient) {
        EntityManager em = DbUtil.getEmfactory().createEntityManager();
        EntityTransaction trans = em.getTransaction();
        trans.begin();
        try {
            em.persist(ingredient);
            trans.commit();
        } catch (Exception e) {
            System.out.println(e);
            trans.rollback();
        } finally {
            em.close();
        }
    }
    /*insert list of ingredients*/

    public static void insertList(ArrayList<Ingredient> ingredients) {
        EntityManager em = DbUtil.getEmfactory().createEntityManager();
        EntityTransaction trans = em.getTransaction();
        trans.begin();
        try {
            for (int i = 0; i < ingredients.size(); i++) {
                em.persist(ingredients.get(i));
            }
            trans.commit();
        } catch (Exception e) {
            System.out.println(e);
            trans.rollback();
        } finally {
            em.close();
        }
    }
    /*Update ingredient*/

    public static void update(Ingredient ingredient) {
        EntityManager em = DbUtil.getEmfactory().createEntityManager();
        EntityTransaction trans = em.getTransaction();
        trans.begin();
        try {
            em.merge(ingredient);
            trans.commit();
        } catch (Exception e) {
            System.out.println(e);
            trans.rollback();
        } finally {
            em.close();
        }
    }
    /*delete ingredient*/

    public static void delete(int ingredient_id) {
        EntityManager em = DbUtil.getEmfactory().createEntityManager();
        EntityTransaction trans = em.getTransaction();
        trans.begin();
        try {
            Ingredient ingredient = em.find(Ingredient.class, ingredient_id);
            em.remove(ingredient);
            trans.commit();
        } catch (Exception e) {
            System.out.println(e);
            trans.rollback();
        } finally {
            em.close();
        }
    }
}