/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.recipes.Db;

import com.recipes.entities.Message;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;


/**
 *
 * @author Sariya
 */
public class MessageDb {

    public static Message getMessageById(int message_id) {
        EntityManager em = DbUtil.getEmfactory().createEntityManager();
        try {
            Message message = em.find(Message.class, message_id);
            return message;
        } finally {
            em.close();
        }

    }

    

    public static List<Message> getAllMessages() {
        EntityManager em = DbUtil.getEmfactory().createEntityManager();
        try {
            return em.createNamedQuery("Message.getAll").getResultList();
        } finally {

            em.close();
        }
    }

    

    public static void insert(Message message) {
        EntityManager em = DbUtil.getEmfactory().createEntityManager();
        EntityTransaction trans = em.getTransaction();
        trans.begin();
        try {
            em.persist(message);
            trans.commit();
        } catch (Exception e) {
            System.out.println(e);
            trans.rollback();
        } finally {
            em.close();
        }
    }

    public static void update(Message message) {
        EntityManager em = DbUtil.getEmfactory().createEntityManager();
        EntityTransaction trans = em.getTransaction();
        trans.begin();
        try {
            em.merge(message);
            trans.commit();
        } catch (Exception e) {
            System.out.println(e);
            trans.rollback();

        } finally {
            em.close();
        }
    }

    public static void delete(int message_id) {
        EntityManager em = DbUtil.getEmfactory().createEntityManager();
        EntityTransaction trans = em.getTransaction();
        trans.begin();
        try {
            Message message = em.find(Message.class, message_id);
            em.remove(message);
            trans.commit();
        } catch (Exception e) {
            System.out.println(e);
            trans.rollback();
        } finally {
            em.close();
        }
    }
}

