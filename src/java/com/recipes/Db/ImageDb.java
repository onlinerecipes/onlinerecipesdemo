/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.recipes.Db;

import com.recipes.entities.Image;
import com.recipes.entities.Recipe;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;
import javax.persistence.NoResultException;
import javax.persistence.TypedQuery;

/**
 *
 * @author Sariya
 */
public class ImageDb {
    /*Fetch image from db by id*/

    public static Image getImageById(int image_id) {
        EntityManager em = DbUtil.getEmfactory().createEntityManager();
        try {
            Image image = em.find(Image.class, image_id);
            return image;
        } finally {
            em.close();
        }

    }
    /*Fetch image from db by image name*/

    public static Image getImageByName(String image_name) {
        EntityManager em = DbUtil.getEmfactory().createEntityManager();
        String query = "select d from Image d  where d.image_name=:image_name";
        TypedQuery<Image> q = em.createQuery(query, Image.class);
        q.setParameter("image_name", image_name);
        Image image = null;
        try {
            image = q.getSingleResult();
        } catch (NoResultException e) {
            System.out.println(e);
             System.out.println("cannot get image");
        } finally {
            em.close();
        }
        return image;
    }
    public static Image getImageByRecipeName(Recipe recipe) {
        EntityManager em = DbUtil.getEmfactory().createEntityManager();
        String query = "select d from Image d  where d.recipe=:recipe";
        TypedQuery<Image> q = em.createQuery(query, Image.class);
        q.setParameter("recipe", recipe);
        List<Image> image = null;
        try {
            image = q.getResultList();
        } catch (NoResultException e) {
            System.out.println(e);
             System.out.println("cannot get image");
        } finally {
            em.close();
        }
        return image.get(0);
    }
    
    /*Fetch all  images*/

    public static List<Image> getAllImages() {
        EntityManager em = DbUtil.getEmfactory().createEntityManager();
        try {
            return em.createNamedQuery("Image.getAll").getResultList();
        } finally {

            em.close();
        }
    }
    /*insert  image*/

    public static void insert(Image image) {
        EntityManager em = DbUtil.getEmfactory().createEntityManager();
        EntityTransaction trans = em.getTransaction();
        trans.begin();
        try {
            em.persist(image);
            trans.commit();
        } catch (Exception e) {
            System.out.println(e);
            trans.rollback();
        } finally {
            em.close();
        }
    }
    /*insert list of images*/

    public static void insertList(List<Image> images) {
        EntityManager em = DbUtil.getEmfactory().createEntityManager();
        EntityTransaction trans = em.getTransaction();
        trans.begin();
        try {
            for (int i = 0; i < images.size(); i++) {
                em.persist(images.get(i));
            }
            trans.commit();
        } catch (Exception e) {
            System.out.println(e);
            trans.rollback();
        } finally {
            em.close();
        }
    }
    /*Update  image*/

    public static void update(Image ingredient) {
        EntityManager em = DbUtil.getEmfactory().createEntityManager();
        EntityTransaction trans = em.getTransaction();
        trans.begin();
        try {
            em.merge(ingredient);
            trans.commit();
        } catch (Exception e) {
            System.out.println(e);
            trans.rollback();
        } finally {
            em.close();
        }
    }
    /*delete  image*/

    public static void delete(int ingredient_id) {
        EntityManager em = DbUtil.getEmfactory().createEntityManager();
        EntityTransaction trans = em.getTransaction();
        trans.begin();
        try {
            Image ingredient = em.find(Image.class, ingredient_id);
            em.remove(ingredient);
            trans.commit();
        } catch (Exception e) {
            System.out.println(e);
            trans.rollback();
        } finally {
            em.close();
        }
    }
}
