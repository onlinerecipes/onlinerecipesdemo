/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.recipes.Db;

import com.recipes.entities.Direction;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;

/**
 *
 * @author Sariya
 */
public class DirectionDb {
    /*Fetch direction from db by id*/
   public static Direction getDirectionById(int direction_id) {
        EntityManager em = DbUtil.getEmfactory().createEntityManager();
        try {
            Direction direction = em.find(Direction.class, direction_id);
            return direction;
        } finally {
            em.close();
        }

    }
    /*Fetch all directions*/

    public static List<Direction> getAllDirections() {
        EntityManager em = DbUtil.getEmfactory().createEntityManager();
        try {
            return em.createNamedQuery("Direction.getAll").getResultList();
        } finally {
             
            em.close();
        }
    }
    /*insert list of  directions*/

    public static void insertList(ArrayList<Direction> directions) {
        EntityManager em = DbUtil.getEmfactory().createEntityManager();
        EntityTransaction trans = em.getTransaction();
        trans.begin();
        try {
            for (int i = 0; i < directions.size(); i++) {
                em.persist(directions.get(i));
            }
            trans.commit();
        } catch (Exception e) {
            System.out.println(e);
            trans.rollback();
        } finally {
            em.close();
        }
    }
    /*insert direction*/

    public static void insert(Direction direction) {
        EntityManager em = DbUtil.getEmfactory().createEntityManager();
        EntityTransaction trans = em.getTransaction();
        trans.begin();
        try {
            em.persist(direction);
            trans.commit();
        } catch (Exception e) {
            System.out.println(e);
            trans.rollback();
        } finally {
            em.close();
        }
    }
    /*Update direction*/

    public static void update(Direction direction) {
        EntityManager em = DbUtil.getEmfactory().createEntityManager();
        EntityTransaction trans = em.getTransaction();
        trans.begin();
        try {
            em.merge(direction);
            trans.commit();
        } catch (Exception e) {
            System.out.println(e);
            trans.rollback();
        } finally {
            em.close();
        }
    }
    /*delete direction*/

    public static void delete(int direction_id) {
        EntityManager em = DbUtil.getEmfactory().createEntityManager();
        EntityTransaction trans = em.getTransaction();
        trans.begin();
        try {
            Direction direction = em.find(Direction.class, direction_id);
            em.remove(direction);
            trans.commit();
        } catch (Exception e) {
            System.out.println(e);
            trans.rollback();
        } finally {
            em.close();
        }
    }
}
