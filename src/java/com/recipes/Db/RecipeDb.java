/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.recipes.Db;

import com.recipes.entities.Category;
import com.recipes.entities.Recipe;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;
import javax.persistence.NoResultException;
import javax.persistence.TypedQuery;

/**
 *
 * @author Sariya
 */
public class RecipeDb {
    /*Fetch recipe from db by id*/

    public static Recipe getRecipeById(int recipe_id) {
        EntityManager em = DbUtil.getEmfactory().createEntityManager();
        try {
            Recipe recipe = em.find(Recipe.class, recipe_id);
            return recipe;
        } finally {
            em.close();
        }

    }

    //get 5 recipes

    public static List<Recipe> getFirstNRecipes(int n) {
        EntityManager em = DbUtil.getEmfactory().createEntityManager();
        String query = "select d from Recipe d order by d.rating desc";
        TypedQuery<Recipe> q = em.createQuery(query, Recipe.class);
        List<Recipe> recipes;
        q.setMaxResults(n);
        try {
            recipes = q.getResultList();
          

        }  finally {
            em.close();
        }
        return recipes;
    }
    /*Fetch recipe  from db by recipe name*/

    public static Recipe getRecipeByName(String recipe_name) {
        EntityManager em = DbUtil.getEmfactory().createEntityManager();
        String query = "select d from Recipe d where d.recipe_name=:recipe_name";
        TypedQuery<Recipe> q = em.createQuery(query, Recipe.class);
        q.setParameter("recipe_name", recipe_name);
        Recipe recipe = null;
        try {
            recipe = q.getSingleResult();
        } catch (NoResultException e) {
            System.out.println(e);
            System.out.println("cannot get recipe");

        } finally {
            em.close();
        }
        return recipe;
    }
    public static List<Recipe> getRecipeByCategoryId(Category id) {
        EntityManager em = DbUtil.getEmfactory().createEntityManager();
        String query = "select d from Recipe d where d.category=:id";
        TypedQuery<Recipe> q = em.createQuery(query, Recipe.class);
        q.setParameter("id", id);
        List<Recipe> recipes = new ArrayList();
        try {
            recipes = q.getResultList();
        } catch (NoResultException e) {
            System.out.println(e);
            System.out.println("cannot get recipe");

        } finally {
            em.close();
        }
        return recipes;
    }
    /*Fetch all recipes*/

    public static List<Recipe> getAllRecipes() {
        EntityManager em = DbUtil.getEmfactory().createEntityManager();
        try {
            return em.createNamedQuery("Recipe.getAll").getResultList();
        } finally {

            em.close();
        }
    }
    /*insert recipe */

    public static void insert(Recipe recipe) {
        EntityManager em = DbUtil.getEmfactory().createEntityManager();
        EntityTransaction trans = em.getTransaction();
        trans.begin();
        try {
            em.persist(recipe);
            trans.commit();
        } catch (Exception e) {
            System.out.println(e);
            trans.rollback();
        } finally {
            em.close();
        }
    }
    /*Update recipe */

    public static void update(Recipe recipe) {
        EntityManager em = DbUtil.getEmfactory().createEntityManager();
        EntityTransaction trans = em.getTransaction();
        trans.begin();
        try {
            em.merge(recipe);
            trans.commit();
        } catch (Exception e) {
            System.out.println(e);
            trans.rollback();
        } finally {
            em.close();
        }
    }
    /*delete recipe */

    public static void delete(int recipe_id) {
        EntityManager em = DbUtil.getEmfactory().createEntityManager();
        EntityTransaction trans = em.getTransaction();
        trans.begin();
        try {
            Recipe recipe = em.find(Recipe.class, recipe_id);
            em.remove(recipe);
            trans.commit();
        } catch (Exception e) {
            System.out.println(e);
            trans.rollback();
        } finally {
            em.close();
        }
    }
}
