/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.recipes.entities;

import com.recipes.utilities.Filter;
import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

/**
 *
 * @author Sariya
 */
@Entity
@Table
@NamedQueries({
    @NamedQuery(name = "Ingredient.getAll", query = "select d from Ingredient d order by d.recipe.recipe_name")})
public class Ingredient implements Serializable {
   /*Ingredient entity which has 5 fields 1 is Foreign key recipe_id*/
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int ingredient_id;
    @Column(nullable = false, length = 255)
    private String ingredient_name;
    @ManyToOne(targetEntity = Recipe.class, fetch = FetchType.EAGER)
    @JoinColumn(name = "recipe_id", nullable = false, referencedColumnName = "recipe_id")
    private Recipe recipe;
    @Column(nullable = false, length = 30)
    private String uom;//Unit of measurement
    @Column(nullable = false)
    private double amount;

    public Ingredient() {

    }
    /*All getter and setter here*/
    public int getIngredient_id() {
        return ingredient_id;
    }

    public void setIngredient_id(int ingredient_id) {
        this.ingredient_id = ingredient_id;
    }

    public String getIngredient_name() {
        return ingredient_name;
    }

    public void setIngredient_name(String ingredient_name) {
        this.ingredient_name = Filter.filter(ingredient_name);
    }

    public Recipe getRecipe() {
        return recipe;
    }

    public void setRecipe(Recipe recipe) {
        this.recipe = recipe;
    }

    public String getUom() {
        return uom;
    }

    public void setUom(String uom) {
        this.uom = Filter.filter(uom);
    }

    public double getAmount() {
        return amount;
    }

    public void setAmount(double amount) {
        this.amount = amount;
    }

}
