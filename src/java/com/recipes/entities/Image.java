/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.recipes.entities;
import java.io.Serializable;
import javax.persistence.*;
/**
 *
 * @author Sariya
 */
@Entity
@Table
@NamedQueries({
    @NamedQuery(name = "Image.getAll", query = "select d from Image d")})
public class Image implements Serializable {
      /*Image entity which has 3 fields 1 is Foreign key recipe_id*/
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int image_id;
    @Column(nullable = false, length = 200)
    private String image_name;
    @ManyToOne(targetEntity = Recipe.class, optional = false, fetch = FetchType.EAGER)
    @JoinColumn(name = "recipe_id", nullable = false,referencedColumnName = "recipe_id")
    private Recipe recipe;
     public Image(){
         
     }
     /*All getter and setter here*/
    public int getImage_id() {
        return image_id;
    }

    public void setImage_id(int image_id) {
        this.image_id = image_id;
    }

    public String getImage_name() {
        return image_name;
    }

    public void setImage_name(String image_name) {
        this.image_name = image_name;
    }

    public Recipe getRecipe() {
        return recipe;
    }

    public void setRecipe(Recipe recipe) {
        this.recipe = recipe;
    }
    
    
}
