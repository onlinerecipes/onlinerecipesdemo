
package com.recipes.entities;
import java.io.Serializable;
import javax.persistence.*;
/**
 *
 * @author Sariya
 */
@Entity
@Table
@NamedQueries({
    @NamedQuery(name = "Comments.getAll", query = "select d from Comments d")})
public class Comments implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    @Column(nullable = false, length = 200)
    private String name;
    @Column(nullable = false, length = 200)
    private String email;
    @Column(nullable = false, length = 2000)
    private String comment;
    @Column(nullable = true)
    private double rank;
    @ManyToOne(targetEntity = Recipe.class, fetch = FetchType.EAGER)
    @JoinColumn(name = "recipe_id", nullable = false, referencedColumnName = "recipe_id")
    private Recipe recipe;
    
    public Comments(){
        
    }
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public double getRank() {
        return rank;
    }

    public void setRank(double rank) {
        this.rank = rank;
    }

    public Recipe getRecipe() {
        return recipe;
    }

    public void setRecipe(Recipe recipe) {
        this.recipe = recipe;
    }

}
