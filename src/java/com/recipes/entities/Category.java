/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.recipes.entities;

import com.recipes.utilities.Filter;
import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

/**
 *
 * @author Sariya
 */
@Entity
@Table
@NamedQueries({
    @NamedQuery(name = "Category.getAll", query = "select d from Category d")})
public class Category implements Serializable {
/* Category entity which has 2 fields id and name*/
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int category_id;
    @Column(nullable = false, unique = true, length = 255)
    private String category_name;

    public Category() {

    }
   /*All getter and setter here*/
    public int getCategory_id() {
        return category_id;
    }

    public void setCategory_id(int category_id) {
        this.category_id = category_id;
    }

    public String getCategory_name() {
        return category_name;
    }

    public void setCategory_name(String category_name) {
        this.category_name = Filter.filter(category_name);
    }
}
