/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.recipes.entities;

import com.recipes.utilities.Filter;
import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

/**
 *
 * @author Sariya
 */
@Entity
@Table
@NamedQueries({
    @NamedQuery(name = "Direction.getAll", query = "select d from Direction d order by d.recipe.recipe_name, d.step")})
public class Direction implements Serializable {
     /*Direction entity which has 4 fields 1 is Foreign key recipe_id*/
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int direction_id;
    @Column(nullable = false)
    private int step;
    @Column(nullable = false, length = 2000)
    private String instruction;
    @ManyToOne(targetEntity = Recipe.class, optional = false, fetch = FetchType.EAGER)
    @JoinColumn(name = "recipe_id", nullable = false, referencedColumnName = "recipe_id")
    private Recipe recipe;
    public Direction(){
        
    }
    /*All getter and setter here*/
    public int getDirection_id() {
        return direction_id;
    }

    public void setDirection_id(int direction_id) {
        this.direction_id = direction_id;
    }

    public int getStep() {
        return step;
    }

    public void setStep(int step) {
        this.step = step;
    }

    public String getInstruction() {
        return instruction;
    }

    public void setInstruction(String instruction) {
        this.instruction = Filter.filter(instruction);
    }

    public Recipe getRecipe() {
        return recipe;
    }

    public void setRecipe(Recipe recipe) {
        this.recipe = recipe;
    }

}
