
package com.recipes.entities;

import com.recipes.utilities.Filter;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;

/**
 *
 * @author Sariya
 */
@Entity
@Table
@NamedQueries({
    @NamedQuery(name = "Recipe.getAll", query = "select d from Recipe d")})
public class Recipe implements Serializable {
/*Recipe entity which has 6 fields and 3 relationships*/
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int recipe_id;
    @Column(nullable = false, unique = true, length = 255)
    private String recipe_name;
    @Column(nullable = true, length = 700)
    private String recipe_desc;
    @Column(nullable = true)
    private int services;
    @Column(nullable = true)
    private int rating;
    @ManyToOne()
    @JoinColumn(name = "category_id")
    private Category category;
    @OneToMany(mappedBy = "recipe", cascade = {CascadeType.PERSIST, CascadeType.REMOVE})
    private List<Ingredient> ingredients = new ArrayList();
    @OneToMany(mappedBy = "recipe", cascade = {CascadeType.PERSIST, CascadeType.REMOVE})
    private List<Direction> directions = new ArrayList();
    @OneToMany(mappedBy = "recipe", cascade = {CascadeType.PERSIST, CascadeType.REMOVE})
    private List<Image> images=new ArrayList(); 
    @OneToMany(mappedBy = "recipe", cascade = {CascadeType.PERSIST, CascadeType.REMOVE})
    private List<Comments> comments=new ArrayList(); 
    public Recipe() {

    }
   /*All getter and setter here*/
    public int getRecipe_id() {
        return recipe_id;
    }

    public void setRecipe_id(int recipe_id) {
        this.recipe_id = recipe_id;
    }

    public String getRecipe_name() {
        return recipe_name;
    }

    public void setRecipe_name(String recipe_name) {
        this.recipe_name = Filter.filter(recipe_name);
    }

    public String getRecipe_desc() {
        return recipe_desc;
    }

    public void setRecipe_desc(String recipe_desc) {
        this.recipe_desc = Filter.filter(recipe_desc);
    }

    public int getServices() {
        return services;
    }

    public void setServices(int services) {
        this.services = services;
    }

    public int getRating() {
        return rating;
    }

    public void setRating(int rating) {
        this.rating = rating;
    }

    public Category getCategory() {
        return category;
    }

    public void setCategory(Category category) {
        this.category = category;
    }

    public List<Ingredient> getIngredients() {
        return ingredients;
    }

    public void setIngredients(List<Ingredient> ingredients) {
        this.ingredients = ingredients;
    }

    public List<Direction> getDirections() {
        return directions;
    }

    public void setDirections(List<Direction> directions) {
        this.directions = directions;
    }

    public List<Image> getImages() {
        
        return images;
    }
     public Image getImage() {
        if(images.isEmpty()){
           return null;
        }
        return images.get(0);
    }
     public List<Image> getImagesForIndividualRecipe() {
        if(images.isEmpty() || images == null)
        return null;
        return images;
    }
    public void setImages(List<Image> images) {
        this.images = images;
    }

    public List<Comments> getComments() {
        return comments;
    }

    public void setComments(List<Comments> comments) {
        this.comments = comments;
    }

   

}
