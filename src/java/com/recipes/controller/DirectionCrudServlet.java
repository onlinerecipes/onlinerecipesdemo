/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.recipes.controller;

import com.recipes.Db.DirectionDb;
import com.recipes.Db.RecipeDb;
import com.recipes.entities.Direction;
import com.recipes.entities.Recipe;
import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author Sariya
 */
public class DirectionCrudServlet extends HttpServlet {

    String Crud = "AdminPnl_Table.jsp";
    String error = "ErrorPage.jsp";
    /*Crud operations for Direction*/

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();
        try {

        } finally {
            out.close();
        }
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();
        try {
            /*delete direction by id*/

            String action = request.getParameter("action");//get action
            String forward = "";
            String direction_idstr = request.getParameter("direction_id");//get direction id
            int direction_id = 0;
            try {
                if (direction_idstr != null || !direction_idstr.isEmpty()) {//if not empty parse to integer
                    direction_id = Integer.parseInt(direction_idstr);
                }
            } catch (NumberFormatException e) {
                System.out.println(e);
            }
            if (action.equals("Delete")) {

                DirectionDb.delete(direction_id);//delete direction by id
                forward = Crud;
            } else {
                forward = Crud;
            }
            response.sendRedirect(forward);
        } catch (Exception e) {
            System.out.println(e);
            response.sendRedirect(error);
        } finally {
            out.close();
        }
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();
        try {
            /*edit direction*/
            String forward = "";
            String direction_idstr = request.getParameter("direction_id");//get direction id
            String stepstr = request.getParameter("step");//get step
            int direction_id = 0;
            int step = 0;
            try {
                if (direction_idstr != null || !direction_idstr.isEmpty()) {//if not empty parse to integer
                    direction_id = Integer.parseInt(direction_idstr);

                }
                if (stepstr != null || !stepstr.isEmpty()) {//if not empty parse to integer
                    step = Integer.parseInt(stepstr);
                }
            } catch (NumberFormatException e) {
                System.out.println(e);
            }

            Direction direction = DirectionDb.getDirectionById(direction_id);//get direction by id from db
            String instruction = request.getParameter("direction");//get edited direction from request
            String recipe_name = request.getParameter("recipe_name");//get recipe_name
            Recipe recipe = RecipeDb.getRecipeByName(recipe_name);//get recipe from db by name
            if (recipe != null && (!instruction.equals("") || instruction != null)) {//if recipe is not null and instruction is not empty
                direction.setInstruction(instruction);//set new direction properties
                direction.setStep(step);
                direction.setRecipe(recipe);
                recipe.getDirections().add(direction);
                recipe.setDirections(recipe.getDirections());
                DirectionDb.update(direction);
                RecipeDb.update(recipe);//update direction
                forward = Crud;

            } else {
                forward = Crud;
            }

            response.sendRedirect(Crud);
        } catch (Exception e) {
            System.out.println(e);
            response.sendRedirect(error);
        } finally {
            out.close();
        }
    }

    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
