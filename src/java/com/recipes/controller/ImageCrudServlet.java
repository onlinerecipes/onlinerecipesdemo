/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.recipes.controller;

import com.recipes.Db.ImageDb;
import com.recipes.Db.RecipeDb;
import com.recipes.entities.Image;
import com.recipes.entities.Recipe;
import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author Sariya
 */
public class ImageCrudServlet extends HttpServlet {

    String Crud = "AdminPnl_Table.jsp";
     String error="ErrorPage.jsp";
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();
        try {
          
        } finally {
            out.close();
        }
    }

    
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();
        try {
            /*delete image by id*/
            String action = request.getParameter("action");//get action
            String forward = "";
            String image_idstr = request.getParameter("image_id");//get id
            int image_id = 0;
            try {
                if (image_idstr != null || !image_idstr.equals("")) {//parse id
                    image_id = Integer.parseInt(image_idstr);
                }
            } catch (NumberFormatException e) {
                System.out.println(e);
            }
            if (action.equals("Delete")) {//if action equals delete

             ImageDb.delete(image_id);//delete image from db by id
                forward = Crud;
            } else {
                forward = Crud;
            }
            response.sendRedirect(forward);
        }catch(Exception e){
             System.out.println(e);
             response.sendRedirect(error);
            }finally {
           
            out.close();
        }
    }

   
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
       response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();
        try {
            /*edit image*/
            String forward = "";
            String image_idstr = request.getParameter("image_id");//get id
           
            int image_id = 0;
          
            try {
                if (image_idstr != null || !image_idstr.equals("")) {//parse id
                   image_id = Integer.parseInt(image_idstr);

                }
               
            } catch (NumberFormatException e) {
                System.out.println(e);
            }

            Image image= ImageDb.getImageById(image_id);//get image from db by id
            System.out.println("image "+image);
            String recipe_name= request.getParameter("recipe_name");//get recipe_name from request
            Recipe recipe = RecipeDb.getRecipeByName(recipe_name);//get recipe from db by name
            System.out.println("recipe "+recipe);
            if (recipe!=null) {//if not null
                image.setRecipe(recipe);//set recipe name
                recipe.getImages().add(image);
                recipe.setImages(recipe.getImages());
                ImageDb.update(image);
                RecipeDb.update(recipe);
                System.out.println("updated recipe");
                forward = Crud;

         } else {
                forward = error;
            }

            response.sendRedirect(Crud);
            }catch(Exception e){
            System.out.println(e);
            response.sendRedirect(error);
        } finally {
            out.close();
        }
    }

   
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
