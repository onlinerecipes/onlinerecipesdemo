/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.recipes.controller;

import com.recipes.Db.MessageDb;
import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author Sariya
 */
public class MessageCrudServlet extends HttpServlet {
    String Crud = "AdminPnl_Table.jsp";
    String error = "ErrorPage.jsp";
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();
        try {
           
        } finally {
            out.close();
        }
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
       response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();
        try {
            /*delete direction by id*/

            String action = request.getParameter("action");//get action
            String forward = "";
            String message_idstr = request.getParameter("message_id");//get message id
            int message_id = 0;
            try {
                if (message_idstr != null || !message_idstr.isEmpty()) {//if not empty parse to integer
                    message_id = Integer.parseInt(message_idstr);
                }
            } catch (NumberFormatException e) {
                System.out.println(e);
            }
            if (action.equals("Delete")) {

                MessageDb.delete(message_id);//delete message by id
                forward = Crud;
            } else {
                forward = Crud;
            }
            response.sendRedirect(forward);
        } catch (Exception e) {
            System.out.println(e);
            response.sendRedirect(error);
        } finally {
            out.close();
        }
    }

   
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
