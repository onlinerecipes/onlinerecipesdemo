
package com.recipes.controller;

import com.recipes.Db.CommentsDb;
import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author Sariya
 */
public class CommentsCrudServlet extends HttpServlet {

  
    String Crud = "AdminPnl_Table.jsp";
    String error = "ErrorPage.jsp";
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();
        try {
        
        } finally {
            out.close();
        }
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
          response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();
        try {
            /*delete direction by id*/

            String action = request.getParameter("action");//get action
            String forward = "";
            String comment_idstr = request.getParameter("comment_id");//get message id
            int comment_id = 0;
            try {
                if (comment_idstr != null || !comment_idstr.isEmpty()) {//if not empty parse to integer
                    comment_id = Integer.parseInt(comment_idstr);
                }
            } catch (NumberFormatException e) {
                System.out.println(e);
            }
            if (action.equals("Delete")) {

                CommentsDb.delete(comment_id);//delete comment by id
                forward = Crud;
            } else {
                forward = Crud;
            }
            response.sendRedirect(forward);
        } catch (Exception e) {
            System.out.println(e);
            response.sendRedirect(error);
        } finally {
            out.close();
        }
    }

   
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
