/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.recipes.controller;

import com.recipes.Db.ImageDb;
import com.recipes.Db.RecipeDb;
import com.recipes.entities.Image;
import com.recipes.entities.Recipe;
import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.Part;

/**
 *
 * @author Sariya
 */
@MultipartConfig(fileSizeThreshold = 1024 * 1024 * 1, // 1 MB
        maxFileSize = 1024 * 1024 * 5, // 5 MB
        maxRequestSize = 1024 * 1024 * 10)
public class NewImageServlet extends HttpServlet {

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();
        try {
            String fileName = null;
            String recipeName = request.getParameter("recipe_name");
            System.out.println(recipeName);
            String saveFile = getServletContext().getInitParameter("file-upload"); //get directory from context param
            //Writing image or file
            Part part = request.getPart("file");
            fileName = getFileName(part);

            Recipe recipe = RecipeDb.getRecipeByName(recipeName);
            if (recipe != null) {
                Image image = new Image();
                image.setImage_name(fileName);
                image.setRecipe(recipe);
                recipe.getImages().add(image);
                recipe.setImages(recipe.getImages());
              
                RecipeDb.update(recipe);
                part.write(saveFile + File.separator + fileName);
                
            } 
                response.setHeader("Access-Control-Allow-Origin", "*");
                response.getWriter().print(fileName + " uploaded successfully");

        } catch (Exception e) {
            System.out.println(e);
            response.getWriter().print(" Failed to upload");
        } finally {
            out.close();
        }
    }

    private String getFileName(Part part) { //honestly i dont know detailly what this code does 
        String contentDisp = part.getHeader("content-disposition");
        System.out.println("content-disposition header= " + contentDisp);
        String[] tokens = contentDisp.split(";");
        for (String token : tokens) {
            if (token.trim().startsWith("filename")) {
                return token.substring(token.indexOf("=") + 2,
                        token.length() - 1);
            }
        }
        return "";
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
