
package com.recipes.controller;

import com.recipes.Db.CommentsDb;
import com.recipes.Db.RecipeDb;
import com.recipes.entities.Comments;
import com.recipes.entities.Recipe;
import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author Konul Gurbanli
 */
public class AddCommentServlet extends HttpServlet {

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
//        response.setContentType("text/html;charset=UTF-8");
       
            if(request.getParameter("test_email")!=null&&!request.getParameter("test_email").equals("")){
                System.out.println("test email :"+ request.getParameter("test_email"));//if it's a spam bot
        response.sendRedirect("UserError.jsp");
        }
            else{
        int recipe_id;        
        int rating=0;
        String name=request.getParameter("name");
        String email=request.getParameter("email");
        String comment=request.getParameter("comment");
        if(request.getParameter("favorite")!=null){
         rating= Integer.parseInt(request.getParameter("favorite"));}
        recipe_id=Integer.parseInt(request.getParameter("recipe_id"));
                System.out.println(recipe_id);
        Recipe recipe = RecipeDb.getRecipeById(recipe_id);
            Comments com= new Comments();
            com.setName(name);
            com.setEmail(email);
            com.setComment(comment);
            com.setRank(rating);
            com.setRecipe(recipe);
            recipe.getComments().add(com);
            if(rating>0){
                int rate=recipe.getRating();
                rate++;
                recipe.setRating(rate);
            }
           RecipeDb.update(recipe);
                System.out.println(recipe.getRating());
           response.sendRedirect("ShowRecipe?recipenm="+recipe_id);
        
//            RequestDispatcher rd= request.getRequestDispatcher("ShowRecipe?recipenm="+recipe_id);
//            rd.forward(request, response);
            }
        
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
