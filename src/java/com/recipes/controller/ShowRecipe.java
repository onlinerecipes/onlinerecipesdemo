
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.recipes.controller;

import com.recipes.Db.RecipeDb;
import com.recipes.entities.Comments;
import com.recipes.entities.Direction;
import com.recipes.entities.Image;
import com.recipes.entities.Ingredient;
import com.recipes.entities.Recipe;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author Natavan
 */
public class ShowRecipe extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();
        try {
            
            int reid = Integer.parseInt(request.getParameter("recipenm")); //selected Recipe id            
            Recipe r = RecipeDb.getRecipeById(reid);
            List<String> recipeDetails = new ArrayList();
            List<Ingredient> ingredients = new ArrayList();
            List<Direction> directions = new ArrayList();
            List<Image> images = new ArrayList();
            List<Comments> comments= new ArrayList();                                          //Assigning elements to lists
            recipeDetails.add(r.getRecipe_name());
            System.out.println("added the name of the recipe to the list");
            recipeDetails.add(r.getCategory().getCategory_name() );
            System.out.println("added the name of the category of the recipe to the list");
            if(r.getRecipe_desc().equals("") || r.getRecipe_desc() == null){
                System.out.println("description is null");
                recipeDetails.add(null);}
            else
            recipeDetails.add(r.getRecipe_desc());
            if(r.getServices() == 0){
                recipeDetails.add(null);
            System.out.println("services is null");}
            else
            recipeDetails.add(Integer.toString(r.getServices()));
            if(r.getRating() == 0){
                recipeDetails.add(null);
                System.out.println("rating is null");}
            else
            recipeDetails.add(Integer.toString(r.getRating()));
            
            ingredients = r.getIngredients();
            directions = r.getDirections();
            
            if(r.getImagesForIndividualRecipe()!= null){
            images = r.getImagesForIndividualRecipe();
            String imgnm = ((Image)images.get(0)).getImage_name();
            request.setAttribute("imgnm",imgnm);
            }
            if(r.getComments()!=null){
                comments=r.getComments();
            }
           //Setting Attributes for JSP to access
            request.setAttribute("details", recipeDetails);
            request.setAttribute("ingredients", ingredients);
            request.setAttribute("directions", directions);
            request.setAttribute("images", images);
            request.setAttribute("comments", comments);
            request.setAttribute("recipe_id", r.getRecipe_id());
            RequestDispatcher rd = request.getRequestDispatcher("IndividualRecipe.jsp");
            rd.forward(request,response);
            
        } finally {
            out.close();
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
