/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.recipes.controller;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Vector;
import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.servlet.http.Part;

/**
 *
 * @author Sariya
 */
@MultipartConfig(fileSizeThreshold = 1024 * 1024 * 1, // 1 MB
        maxFileSize = 1024 * 1024 * 5, // 5 MB
        maxRequestSize = 1024 * 1024 * 10)
// 10 MB
public class UploadFile extends HttpServlet {

        ArrayList<String> links; 
    
    private static final long serialVersionUID = 1L;

    @Override
    protected void doPost(HttpServletRequest request,
            HttpServletResponse response) throws ServletException, IOException {
        try {
            HttpSession session = request.getSession(false); //get already created session in AdminPnl.jsp
            String fileName = null;
            String saveFile=getServletContext().getInitParameter("file-upload"); //get directory from context param
            //Writing image or file
            Part part = request.getPart("file");
            fileName = getFileName(part);
            if(!fileName.isEmpty()){
            links.add(fileName); //add element to list
            }
            System.out.println(links);
            session.setAttribute("links", links); //set links as an attribute
            part.write(saveFile + File.separator + fileName);
   
            // Extra logic to support multiple domain 
            response.setHeader("Access-Control-Allow-Origin", "*");
            response.getWriter().print(fileName + " uploaded successfully");
        } catch (Exception e) {
            System.out.println(e);
            response.getWriter().print(" Nothing to upload");
        }
    }
   //get the name of uploaded file
    private String getFileName(Part part) { //honestly i dont know detailly what this code does 
        String contentDisp = part.getHeader("content-disposition");
        System.out.println("content-disposition header= " + contentDisp);
        String[] tokens = contentDisp.split(";");
        for (String token : tokens) {
            if (token.trim().startsWith("filename")) {
                return token.substring(token.indexOf("=") + 2,
                        token.length() - 1);
            }
        }
        return "";
    }

    @Override
    public void init() throws ServletException {
        super.init(); //To change body of generated methods, choose Tools | Templates.
        links = new ArrayList();
    }

}
