/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.recipes.controller;

import com.recipes.Db.IngredientDb;
import com.recipes.Db.RecipeDb;
import com.recipes.entities.Ingredient;
import com.recipes.entities.Recipe;
import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author Sariya
 */
public class IngredientCrudServlet extends HttpServlet {

    String Crud = "AdminPnl_Table.jsp";
    String error = "ErrorPage.jsp";
    /*Ingredient crud operations*/
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();
        try {

        } finally {
            out.close();
        }
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();
        try {
            String action = request.getParameter("action");//get action from request
            String forward = "";
            String ing_idstr = request.getParameter("ingredient_id");//get ingredient_id
            int ing_id = 0;
            try {
                if (ing_idstr != null || !ing_idstr.equals("")) { //if not null parse to integer
                    ing_id = Integer.parseInt(ing_idstr);
                }
            } catch (NumberFormatException e) {
                System.out.println(e);
            }
            if (action.equals("Delete")) {
                System.out.println("delete");
                IngredientDb.delete(ing_id); //delete ingredient by id
                forward = Crud;
            } else {
                forward = Crud;
            }
            response.sendRedirect(forward);
        } catch (Exception e) {
            System.out.println(e);
            response.sendRedirect(error);
        } finally {

            out.close();

        }
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();
        try {
            String forward = "";
            String ing_idstr = request.getParameter("ingredient_id");//get ingredient id
            String amountstr = request.getParameter("amount");// get amount
            int ing_id = 0;
            double amount = 0;
            try {
                if (ing_idstr != null || !ing_idstr.equals("")) { //if not null parse to integer
                    ing_id = Integer.parseInt(ing_idstr);

                }
                if (amountstr != null || !amountstr.equals("")) {//if not null parse to double
                    amount = Double.parseDouble(amountstr);
                }
            } catch (NumberFormatException e) {
                System.out.println(e);
            }

            Ingredient ingredient = IngredientDb.getIngredientById(ing_id); //get ingredient from db by id
            String ingredient_name = request.getParameter("ingredient_name"); //get edited ingredient name from request
            String recipe_name = request.getParameter("recipe_name"); //get edited recipe name from request
            String uom = request.getParameter("uom"); //get uom

            Recipe recipe = RecipeDb.getRecipeByName(recipe_name); //get recipe from db by name

            if (!ingredient_name.equals("") && recipe != null && !uom.equals("")) { //if not null
                ingredient.setIngredient_name(ingredient_name); //set ingredient properties
                ingredient.setRecipe(recipe);
                ingredient.setAmount(amount);
                ingredient.setUom(uom);
                recipe.getIngredients().add(ingredient);
                recipe.setIngredients(recipe.getIngredients());
                IngredientDb.update(ingredient);//update ingredient
                RecipeDb.update(recipe); 
                forward = Crud;

            } else {
                forward = Crud;
            }

            response.sendRedirect(forward);
        } catch (Exception e) {
            System.out.println(e);
            response.sendRedirect(error);

        } finally {

            out.close();

        }

    }

    @Override
    public String getServletInfo() {
        return "Short description";
    }

    // </editor-fold>
}
