/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.recipes.controller;

import com.recipes.Db.MessageDb;
import com.recipes.entities.Message;
import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author Sariya
 */
public class AddMessageServlet extends HttpServlet {

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();
        try {
        if (request.getParameter("userName") == null || request.getParameter("userEmail") == null
                    || request.getParameter("userMessage") == null || (request.getParameter("userName").equals("")
                    || request.getParameter("userEmail").equals("") || request.getParameter("userMessage").equals(""))) {
                System.out.println("null");
                out.println("<script type=\"text/javascript\">");   //alert when necessary field is null
                out.println("alert('Fill necessary fields');");
                out.println("location='main.jsp';");
                out.println("</script>");

            } else {
            /*Get all necessary Params for adding message*/
            String name=request.getParameter("userName");
            String email=request.getParameter("userEmail");
            String message=request.getParameter("userMessage");
            
            /*create new instance of message entity and set all properties*/
            Message message1=new Message();
            message1.setEmail(email);
            message1.setMessage(message);
            message1.setName(name);
            MessageDb.insert(message1);
        }
            response.sendRedirect("main.jsp");
           
        }catch(Exception e){
            System.out.println(e);
            response.sendRedirect("main.jsp");
        
        } finally {
            out.close();
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
