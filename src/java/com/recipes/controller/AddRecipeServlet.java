/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.recipes.controller;

import com.recipes.entities.*;
import com.recipes.Db.*;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author Sariya
 */
public class AddRecipeServlet extends HttpServlet {
    /*Add all recipe fields*/

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();
        try {
            //check for nullity of request params.
            if (request.getParameter("recipe_name") == null || request.getParameter("direction") == null
                    || request.getParameter("ingredients") == null || (request.getParameter("recipe_name").equals("")
                    || request.getParameter("ingredients").equals("") || request.getParameter("direction").equals(""))) {
                System.out.println("null");
                out.println("<script type=\"text/javascript\">");   //alert when necessary field is null
                out.println("alert('Fill necessary fields');");
                out.println("location='AdminPnl.jsp';");
                out.println("</script>");

            } else {
                HttpSession session = request.getSession(false); //session for getting image name
                List<String> links = (ArrayList) session.getAttribute("links");
                System.out.println(links);
                int services = 0;

                String recipe_name = request.getParameter("recipe_name");//for recipeDb, recipe_name field
                String recipe_desc = request.getParameter("recipe_desc");//for recipeDb, recipe_description

                String category = request.getParameter("category"); //for categoryDb , category name

                if (!request.getParameter("services").isEmpty() || !request.getParameter("services").equals("")) {//if not null parse to integer
                    services = Integer.parseInt(request.getParameter("services"));//for recipedb
                }
                System.out.println("recipe_name: " + recipe_name + " desc: " + recipe_desc + " categ: "
                        + category + " serv: " + services);

                String[] ingredients = request.getParameterValues("ingredients");//for ingredienddb, set ingredients to array;

                String[] amounts = request.getParameterValues("amount");//for ingredienddb, set amounts to array;

                //initialize an array size of amounts
                double amount[] = new double[amounts.length];
                for (int i = 0; i < amount.length; i++) {
                    amount[i] = 0;
                }
                //if not null, parse amounts to int
                for (int i = 0; i < amounts.length; i++) {
                    if (!amounts[i].isEmpty()||amounts[i] != null || !amounts[i].equals("")) {
                        amount[i] = Double.parseDouble(amounts[i]);
                    }
                }

                String[] uom = request.getParameterValues("uom");//for ingredientDb, set uoms to array

                String[] steps = request.getParameterValues("step");//for directionDb, set steps to array

                //initialize an array of steps length
                int step[] = new int[steps.length];
                for (int i = 0; i < step.length; i++) {
                    step[i] = 0;
                }
                //if not null parse to integer
                for (int i = 0; i < steps.length; i++) {
                    if (!steps[i].isEmpty()||steps[i] != null || !steps[i].equals("")) {
                        step[i] = Integer.parseInt(steps[i]);
                    }
                }

                String[] direction = request.getParameterValues("direction");//For DirectionDb, set directions to array

                //set properties for Category
                Category categ = CategoryDb.getCategoryByName(category);
                if (categ == null) {
                    System.out.println("categ==null");
                    categ = new Category();
                    categ.setCategory_name(category);
                    CategoryDb.insert(categ);

                }

                //set properties for recipe
                Recipe recipe = new Recipe();
                recipe.setRecipe_name(recipe_name);
                recipe.setRecipe_desc(recipe_desc);
                recipe.setServices(services);
                recipe.setCategory(categ);

                //set properties for directions
                for (int i = 0; i < step.length; i++) {
                    if ((direction[i] != null) || (direction[i].length() != 0)) {
                        Direction direct = new Direction();
                        direct.setStep(step[i]);
                        direct.setInstruction(direction[i]);
                        direct.setRecipe(recipe);
                        recipe.getDirections().add(direct);
                        System.out.println("set " + recipe.getDirections());
                    }
                }

                //set properties for ingredients
                for (int i = 0; i < amount.length; i++) {
                    if ((!ingredients[i].isEmpty()||ingredients[i] != null && uom[i] != null)
                            || (ingredients[i].length() != 0 && uom[i].length() != 0)) {
                        Ingredient ingredient = new Ingredient();
                        ingredient.setIngredient_name(ingredients[i]);
                        ingredient.setAmount(amount[i]);
                        ingredient.setUom(uom[i]);
                        ingredient.setRecipe(recipe);
                        recipe.getIngredients().add(ingredient);
                        System.out.println("ingredients added");
                    }
                }

                //set Properties for image
                if (links != null) { //iterate thorugh arraylist and set image links to prescription
                    for (int i = 0; i < links.size(); i++) {
                        if(!links.get(i).isEmpty()){
                        Image image = new Image();
                        image.setImage_name(links.get(i));
                        image.setRecipe(recipe);
                        recipe.getImages().add(image);
                        }
                    }

                    session.removeAttribute("links");//remove attribute from session
                } else {
                    System.out.println("No image to set");
                }
                System.out.println("ings " + recipe.getIngredients());
                System.out.println(" direcs " + recipe.getDirections());
                System.out.println("images " + recipe.getImages());
                //map all lists to recipe
                recipe.setIngredients(recipe.getIngredients());
                recipe.setDirections(recipe.getDirections());
                recipe.setImages(recipe.getImages());
                //insert all to db
                RecipeDb.insert(recipe);
                response.sendRedirect("AdminPnl_Table.jsp");
            }

        } catch (Exception e) {
            System.out.println(e);
            response.sendRedirect("ErrorPage.jsp");
        } finally {
            out.close();
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
