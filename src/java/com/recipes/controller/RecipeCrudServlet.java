/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.recipes.controller;

import com.recipes.Db.CategoryDb;
import com.recipes.Db.RecipeDb;
import com.recipes.entities.Category;
import com.recipes.entities.Recipe;
import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author Sariya
 */
public class RecipeCrudServlet extends HttpServlet {

 
  
    String Crud = "AdminPnl_Table.jsp";
     String error="ErrorPage.jsp";
    /*Recipe crud operation*/
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();
        try {

        } finally {
            out.close();
        }
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();
        try {
            String action = request.getParameter("action");//get action from request
            String forward = "";
            String recipe_idstr = request.getParameter("recipe_id"); //get recipe id from request
            int recipe_id = 0;
            try {
                /*if not null parse to integer*/
                if (recipe_idstr != null || !recipe_idstr.equals("")) {
                    recipe_id = Integer.parseInt(recipe_idstr);
                }
            } catch (NumberFormatException e) {
                System.out.println(e);
            }
            if (action.equals("Delete")) {

                RecipeDb.delete(recipe_id);
                forward = Crud;
            } else {
                forward = Crud;
            }
             response.sendRedirect(forward);
        }catch(Exception e){
            System.out.println(e);
            response.sendRedirect(error);
        }finally {
            out.close();
        }
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();
        try {
                  /*if not null parse to integer*/
            String forward = "";
            String recipe_idstr = request.getParameter("recipe_id"); //get recipe id
            String services = request.getParameter("services"); //get services
            int recipe_id = 0;
            int service = 0;
            try {
                if (recipe_idstr != null || !recipe_idstr.equals("")) { 
                    recipe_id = Integer.parseInt(recipe_idstr);

                }
                if (services != null || !services.equals("")) {
                    service = Integer.parseInt(services);
                }
            } catch (NumberFormatException e) {
                System.out.println(e);
            }

            Recipe recipe = RecipeDb.getRecipeById(recipe_id);//get recipe from db
            System.out.println(recipe);
            String recipe_name = request.getParameter("recipe_name"); //get recipe name from request
            String recipe_desc = request.getParameter("recipe_desc"); //get recipe description from request 
            String category = request.getParameter("category"); //get category from request
            Category categ = CategoryDb.getCategoryByName(category); //find category in db by name
            
            if (!recipe_name.equals("")&&categ!=null) { //if not null
                recipe.setRecipe_name(recipe_name); //set properties
                recipe.setServices(service);
                recipe.setRecipe_desc(recipe_desc);
                recipe.setCategory(categ);
                RecipeDb.update(recipe); //update recipe 
                forward = Crud;

            } else {
                forward = Crud;
            }

            response.sendRedirect(Crud);
            }catch(Exception e){
            System.out.println(e);
            response.sendRedirect(error);
        } finally {
            out.close();
        }
    }

    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
