/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.recipes.controller;

import com.recipes.Db.RecipeDb;
import com.recipes.entities.Direction;
import com.recipes.entities.Recipe;
import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author Sariya
 */
public class NewDirectionServlet extends HttpServlet {

   
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();
        try {
         if (request.getParameter("step") == null || request.getParameter("direction") == null
                    || request.getParameter("recipe_name") == null || (request.getParameter("step").equals("")
                    || request.getParameter("direction").equals("") || request.getParameter("recipe_name").equals(""))) {
                System.out.println("null");
                out.println("<script type=\"text/javascript\">");   //alert when necessary field is null
                out.println("alert('Fill necessary fields');");
                out.println("location='AdminPnl_Table.jsp';");
                out.println("</script>");

            } else {
                String direct = request.getParameter("direction");
                String RecipeName = request.getParameter("recipe_name");
                String stepstr=request.getParameter("step");
                System.out.println(RecipeName);
                int step = 0;
                if (stepstr != null || !stepstr.equals("")) {//if not null parse to integer
                    step = Integer.parseInt(stepstr);
                }
                Recipe recipe = RecipeDb.getRecipeByName(RecipeName);
                if (recipe != null) {
                  Direction direction=new Direction();
                  direction.setInstruction(direct);
                  direction.setStep(step);
                  direction.setRecipe(recipe);
                  recipe.getDirections().add(direction);
                  recipe.setDirections(recipe.getDirections());
                  RecipeDb.update(recipe);
                }

            }
             response.sendRedirect("AdminPnl_Table.jsp");
           
        }catch(Exception e){
            System.out.println(e);
            response.sendRedirect("ErrorPage.jsp");
        } 
        finally {
            out.close();
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
