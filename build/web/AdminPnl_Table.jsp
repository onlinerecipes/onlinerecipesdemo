<%-- 
    Document   : AdminPnl_Table
    Created on : Mar 31, 2016, 9:11:15 PM
    Author     : Natavan
--%>
<%@page errorPage="ErrorPage.jsp"%>
<%@include file="loginCheck.jsp" %> 
<%@page import="com.recipes.Db.*"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@page errorPage="ErrorPage.jsp"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7" crossorigin="anonymous">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js" integrity="sha384-0mSbJDEHialfmuBBQP6A4Qrprq5OVfW37PRR3j5ELqxss1yVqOtnepnHVP9aJ7xS" crossorigin="anonymous"></script>

        <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7" crossorigin="anonymous">
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js" integrity="sha384-0mSbJDEHialfmuBBQP6A4Qrprq5OVfW37PRR3j5ELqxss1yVqOtnepnHVP9aJ7xS" crossorigin="anonymous"></script>
        <title>Admin Panel</title>

    </head>

    <body>
        <style>           
            a:hover{
                text-decoration: none;
            }
            .search-query{
                margin-right: 30px;
            }
            ul{
                list-style: none;
            }
            ul li{
                float: left;
                margin:10px 10px 0px;
            }
            .navbar-inverse{           
                margin-bottom:0;
                margin-top:22%;
            }
        </style>
        <c:import url="/WEB-INF/tags/adminHeader.tag"/>

        <div class="ch-container">
            <div class="row">

                <!-- left menu -->
                <div class="col-sm-2 col-lg-2">
                    <div class="sidebar-nav">
                        <div class="nav-canvas">
                            <div class="nav-sm nav nav-stacked">

                            </div>
                            <ul class="collapse navbar-collapse nav nav-pills nav-stacked main-menu">
                                <li class="nav-header"></li>
                                <li><a class="ajax-link" href="AdminPnl.jsp"><i class="glyphicon glyphicon-home"></i><span> Dashboard</span></a>
                                </li>
                                <li><a class="ajax-link" href="#"><i class="glyphicon glyphicon-list-alt"></i><span> Charts</span></a>
                                </li>
                                <li class="active"><a class="ajax-link" href="AdminPnl_Table.jsp"><i
                                            class="glyphicon glyphicon-align-justify"></i><span> Tables</span></a></li>                        
                                <li><a class="ajax-link" href="#"><i class="glyphicon glyphicon-calendar"></i><span> Calendar</span></a>
                                </li>
                            </ul>                    
                        </div>
                    </div>
                </div>

                <noscript>
                <div class="alert alert-block col-md-12">
                    <h4 class="alert-heading">Warning!</h4>

                    <p>You need to have <a href="http://en.wikipedia.org/wiki/JavaScript" target="_blank">JavaScript</a>
                        enabled to use this site.</p>
                </div>
                </noscript>

                <div id="content" class="col-lg-10 col-sm-10">
                    <div>
                        <ul class="breadcrumb">
                            <li>
                                <a href="#">Home</a>
                            </li>
                            <li>
                                <a href="#">Tables</a>
                            </li>
                        </ul>
                    </div>

                    <div class="row">
                        <div class="box col-md-12">
                            <div class="box-inner">
                                <div class="box-header well" data-original-title="">
                                    <h2><i class="glyphicon glyphicon-apple"></i> Recipe</h2>
                                    <div class="box-icon">
                                        <a href="#" class="btn btn-setting btn-round btn-default"><i class="glyphicon glyphicon-cog"></i></a>
                                        <a href="#" class="btn btn-minimize btn-round btn-default"><i
                                                class="glyphicon glyphicon-chevron-up"></i></a>
                                        <a href="#" class="btn btn-close btn-round btn-default"><i class="glyphicon glyphicon-remove"></i></a>
                                    </div>

                                </div>
                                <div class="box-content">
                                    <%
                                        request.setAttribute("allCategories", CategoryDb.getAllCategories());
                                        request.setAttribute("allRecipes", RecipeDb.getAllRecipes());
                                        request.setAttribute("allIngredients", IngredientDb.getAllIngredients());
                                        request.setAttribute("allDirections", DirectionDb.getAllDirections());
                                        request.setAttribute("allImages", ImageDb.getAllImages());
                                        request.setAttribute("allComments", CommentsDb.getAllComments());
                                        request.setAttribute("allMessages", MessageDb.getAllMessages());
                                    %>

                                    <table class="table table-striped" id="recipe_table">
                                        <thead>
                                            <tr>
                                                <th>Recipe ID</th>
                                                <th>Recipe Name</th>
                                                <th>Recipe Description</th>
                                                <th>Services</th>
                                                <th>Category</th>
                                                <th>Rating</th>
                                                <th>Actions</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <c:forEach items="${allRecipes}" var="rec">
                                                <tr>

                                                    <td class="center" id='recipeId'><c:out value="${rec.recipe_id}"/></td>
                                                    <td class="center" id="recipeName"><c:out value="${rec.recipe_name}"/></td>
                                                    <td class="center" id='recipeDesc'><c:out value="${rec.recipe_desc}"/></td>
                                                    <td class="center" id='services'><c:out value="${rec.services}"/></td>
                                                    <td class="center" id='category'><c:out value="${rec.getCategory().getCategory_name()}"/></td>
                                                    <td class="center" id='rating'><c:out value="${rec.rating}"/></td>

                                                    <td class="center" >           
                                                        <a data-toggle="model" class="btn btn-info recipe-edit" href="" data-id0='${rec.recipe_id}'  data-id="${rec.recipe_name}" data-id2="${rec.recipe_desc}" data-id4='${rec.services}' data-id5='${rec.getCategory().getCategory_name()}' >
                                                            <i class="glyphicon glyphicon-edit icon-white"></i>
                                                            Edit
                                                        </a>
                                                        <a class="btn btn-danger" href="RecipeCrudServlet?action=Delete&recipe_id=<c:out value="${rec.recipe_id}"/>"/>
                                                        <i class="glyphicon glyphicon-trash icon-white"></i>
                                                        Delete
                                                        </a>
                                                    </td>
                                                </tr>
                                            </c:forEach>
                                        </tbody>
                                    </table>
                                    <ul class="pagination pagination-centered">
                                        <li><a href="#">Prev</a></li>
                                        <li class="active">
                                            <a href="#">1</a>
                                        </li>
                                        <li><a href="#">2</a></li>
                                        <li><a href="#">3</a></li>
                                        <li><a href="#">4</a></li>
                                        <li><a href="#">Next</a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>

                    </div>

                    <div class="row">
                        <div class="box col-md-12">
                            <div class="box-inner">
                                <div class="box-header well" data-original-title="">
                                    <h2><i class="glyphicon glyphicon-apple"></i>Ingredient</h2>

                                    <div class="box-icon">
                                        <a href="#" class="btn btn-setting btn-round btn-default"><i
                                                class="glyphicon glyphicon-cog"></i></a>
                                        <a href="#" class="btn btn-minimize btn-round btn-default"><i
                                                class="glyphicon glyphicon-chevron-up"></i></a>
                                        <a href="#" class="btn btn-close btn-round btn-default"><i
                                                class="glyphicon glyphicon-remove"></i></a>
                                    </div>
                                    </br>
                                    <a data-toggle="model" class="btn btn-default ingredient-add" >
                                        <i class="glyphicon glyphicon-edit icon-white"></i>
                                        Add new ingredient
                                    </a>
                                </div>
                                <div class="box-content">
                                    <table class="table table-striped">
                                        <thead>
                                            <tr>
                                                <th>Recipe</th>
                                                <th>Ingredient Name</th>
                                                <th>UOM</th>
                                                <th>Amount</th>
                                                <th>Actions</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <c:forEach items="${allIngredients}" var="ing">
                                                <tr>

                                                    <td class="center">${ing.getRecipe().getRecipe_name()}</td>
                                                    <td class="center">${ing.ingredient_name}</td>
                                                    <td class="center">${ing.uom}</td>
                                                    <td class="center">${ing.amount}</td>
                                                    <td class="center">
                                                        <a data-toggle="model" class="btn btn-info ingredient-edit" data-ingredientid="${ing.ingredient_id}" data-ingredientname="${ing.ingredient_name}" data-recipename="${ing.getRecipe().getRecipe_name()}" data-recipeid="${ing.getRecipe().getRecipe_id()}" data-uom="${ing.uom}" data-amount="${ing.amount}">
                                                            <i class="glyphicon glyphicon-edit icon-white"></i>
                                                            Edit
                                                        </a>
                                                        <a class="btn btn-danger" href="IngredientCrudServlet?action=Delete&ingredient_id=<c:out value="${ing.ingredient_id}"/>">
                                                            <i class="glyphicon glyphicon-trash icon-white"></i>
                                                            Delete
                                                        </a>
                                                    </td>
                                                </tr>
                                            </c:forEach>


                                        </tbody>
                                    </table>
                                    <ul class="pagination pagination-centered">
                                        <li><a href="#">Prev</a></li>
                                        <li class="active">
                                            <a href="#">1</a>
                                        </li>
                                        <li><a href="#">2</a></li>
                                        <li><a href="#">3</a></li>
                                        <li><a href="#">4</a></li>
                                        <li><a href="#">Next</a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="box col-md-12">
                            <div class="box-inner">
                                <div class="box-header well" data-original-title="">
                                    <h2><i class="glyphicon glyphicon-apple"></i>Direction</h2>

                                    <div class="box-icon">
                                        <a href="#" class="btn btn-setting btn-round btn-default"><i
                                                class="glyphicon glyphicon-cog"></i></a>
                                        <a href="#" class="btn btn-minimize btn-round btn-default"><i
                                                class="glyphicon glyphicon-chevron-up"></i></a>
                                        <a href="#" class="btn btn-close btn-round btn-default"><i
                                                class="glyphicon glyphicon-remove"></i></a>
                                    </div>
                                    </br>
                                    <a data-toggle="model" class="btn btn-default direction-add" >
                                        <i class="glyphicon glyphicon-edit icon-white"></i>
                                        Add a new instruction
                                    </a>
                                </div>
                                <div class="box-content">
                                    <table class="table table-striped">
                                        <thead>
                                            <tr>
                                                <th>Recipe</th>
                                                <th>Step </th>
                                                <th>Direction</th>


                                            </tr>
                                        </thead>
                                        <tbody>
                                            <c:forEach items="${allDirections}" var="dir">
                                                <tr>
                                                    <td class="center">${dir.getRecipe().getRecipe_name()}</td>
                                                    <td class="center">${dir.step}</td>
                                                    <td class="center">${dir.instruction}</td>


                                                    <td class="center">
                                                        <a data-toggle="model" class="btn btn-info direction-edit" data-directionid="${dir.direction_id}" data-step="${dir.step}" data-direction="${dir.instruction}" data-recipename="${dir.getRecipe().getRecipe_name()}" >
                                                            <i class="glyphicon glyphicon-edit icon-white"></i>
                                                            Edit
                                                        </a>
                                                        <a class="btn btn-danger" href="DirectionCrudServlet?action=Delete&direction_id=<c:out value="${dir.direction_id}"/>">
                                                            <i class="glyphicon glyphicon-trash icon-white"></i>
                                                            Delete
                                                        </a>
                                                    </td>
                                                </tr>
                                            </c:forEach>


                                        </tbody>
                                    </table>
                                    <ul class="pagination pagination-centered">
                                        <li><a href="#">Prev</a></li>
                                        <li class="active">
                                            <a href="#">1</a>
                                        </li>
                                        <li><a href="#">2</a></li>
                                        <li><a href="#">3</a></li>
                                        <li><a href="#">4</a></li>
                                        <li><a href="#">Next</a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="box col-md-12">
                            <div class="box-inner">
                                <div class="box-header well" data-original-title="">
                                    <h2><i class="glyphicon glyphicon-camera"></i>Image</h2>

                                    <div class="box-icon">
                                        <a href="#" class="btn btn-setting btn-round btn-default"><i
                                                class="glyphicon glyphicon-cog"></i></a>
                                        <a href="#" class="btn btn-minimize btn-round btn-default"><i
                                                class="glyphicon glyphicon-chevron-up"></i></a>
                                        <a href="#" class="btn btn-close btn-round btn-default"><i
                                                class="glyphicon glyphicon-remove"></i></a>
                                    </div>
                                    </br>
                                    <a data-toggle="model" class="btn btn-default image-add" >
                                        <i class="glyphicon glyphicon-camera icon-white"></i>
                                        Add a new image
                                    </a>
                                </div>
                                <div class="box-content">
                                    <table class="table table-striped">
                                        <thead>
                                            <tr>

                                                <th>Recipe Name</th>
                                                <th>Image</th>
                                                <th>Actions</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <c:forEach items="${allImages}" var="img">
                                                <tr>

                                                    <td class="center">${img.getRecipe().getRecipe_name()}</td>
                                                    <td class="center"><img src="uploaded-images/${img.image_name}" alt="image" style="width: 100px; height: 100px;"></td>

                                                    <td class="center">
                                                        <a data-toggle="model" class="btn btn-info image-edit" data-imageid="${img.image_id}" data-recipename="${img.getRecipe().getRecipe_name()}">
                                                            <i class="glyphicon glyphicon-edit icon-white"></i>
                                                            Edit
                                                        </a>
                                                        <a class="btn btn-danger" href="ImageCrudServlet?action=Delete&image_id=<c:out value="${img.image_id}"/>">
                                                            <i class="glyphicon glyphicon-trash icon-white"></i>
                                                            Delete
                                                        </a>
                                                    </td>
                                                </tr>
                                            </c:forEach>


                                        </tbody>
                                    </table>
                                    <ul class="pagination pagination-centered">
                                        <li><a href="#">Prev</a></li>
                                        <li class="active">
                                            <a href="#">1</a>
                                        </li>
                                        <li><a href="#">2</a></li>
                                        <li><a href="#">3</a></li>
                                        <li><a href="#">4</a></li>
                                        <li><a href="#">Next</a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>          
                    <div class="row">
                        <div class="box col-md-12">
                            <div class="box-inner">
                                <div class="box-header well" data-original-title="">
                                    <h2><i class="glyphicon glyphicon-apple"></i>Comments</h2>

                                    <div class="box-icon">
                                        <a href="#" class="btn btn-setting btn-round btn-default"><i
                                                class="glyphicon glyphicon-cog"></i></a>
                                        <a href="#" class="btn btn-minimize btn-round btn-default"><i
                                                class="glyphicon glyphicon-chevron-up"></i></a>
                                        <a href="#" class="btn btn-close btn-round btn-default"><i
                                                class="glyphicon glyphicon-remove"></i></a>
                                    </div>
                                </div>
                                <div class="box-content">
                                    <table class="table table-striped">
                                        <thead>
                                            <tr>
                                                <th>Recipe</th>
                                                <th>Comment </th>
                                                <th>User Name</th>
                                                <th>User email</th>

                                            </tr>
                                        </thead>
                                        <tbody>
                                            <c:forEach items="${allComments}" var="com">
                                                <tr>
                                                    <td class="center">${com.getRecipe().getRecipe_name()}</td>
                                                    <td class="center">${com.getComment()}</td>
                                                    <td class="center">${com.getName()}</td>
                                                    <td class="center">${com.getEmail()}</td>

                                                    <td class="center">

                                                        <a class="btn btn-danger" href="CommentsCrudServlet?action=Delete&comment_id=<c:out value="${com.getId()}"/>">
                                                            <i class="glyphicon glyphicon-trash icon-white"></i>
                                                            Delete
                                                        </a>
                                                    </td>
                                                </tr>
                                            </c:forEach>


                                        </tbody>
                                    </table>
                                    <ul class="pagination pagination-centered">
                                        <li><a href="#">Prev</a></li>
                                        <li class="active">
                                            <a href="#">1</a>
                                        </li>
                                        <li><a href="#">2</a></li>
                                        <li><a href="#">3</a></li>
                                        <li><a href="#">4</a></li>
                                        <li><a href="#">Next</a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                         <div class="row">
                        <div class="box col-md-12">
                            <div class="box-inner">
                                <div class="box-header well" data-original-title="">
                                    <h2><i class="glyphicon glyphicon-apple"></i>Messages</h2>

                                    <div class="box-icon">
                                        <a href="#" class="btn btn-setting btn-round btn-default"><i
                                                class="glyphicon glyphicon-cog"></i></a>
                                        <a href="#" class="btn btn-minimize btn-round btn-default"><i
                                                class="glyphicon glyphicon-chevron-up"></i></a>
                                        <a href="#" class="btn btn-close btn-round btn-default"><i
                                                class="glyphicon glyphicon-remove"></i></a>
                                    </div>
                                </div>
                                <div class="box-content">
                                    <table class="table table-striped">
                                        <thead>
                                            <tr>
                                                
                                                <th>Message</th>
                                                <th>User Name</th>
                                                <th>User email</th>

                                            </tr>
                                        </thead>
                                        <tbody>
                                            <c:forEach items="${allMessages}" var="mes">
                                                <tr>
                                                  
                                                    <td class="center">${mes.getMessage()}</td>
                                                    <td class="center">${mes.getName()}</td>
                                                    <td class="center">${mes.getEmail()}</td>

                                                    <td class="center">

                                                        <a class="btn btn-danger" href="MessageCrudServlet?action=Delete&message_id=<c:out value="${mes.getId()}"/>">
                                                            <i class="glyphicon glyphicon-trash icon-white"></i>
                                                            Delete
                                                        </a>
                                                    </td>
                                                </tr>
                                            </c:forEach>


                                        </tbody>
                                    </table>
                                    <ul class="pagination pagination-centered">
                                        <li><a href="#">Prev</a></li>
                                        <li class="active">
                                            <a href="#">1</a>
                                        </li>
                                        <li><a href="#">2</a></li>
                                        <li><a href="#">3</a></li>
                                        <li><a href="#">4</a></li>
                                        <li><a href="#">Next</a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                    <footer>
                        <div class="navbar navbar-inverse">
                            <div class="container">
                                <p class="copyright navbar-text pull-left">© 2016 Granny's Kitchen</p>
                            </div>
                        </div>
                    </footer>

                </div>

                <div class="modal fade" id="ingredientEdit" aria-hidden="true">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal">×</button>
                                <h3>Editing</h3>
                            </div>
                            <div class="modal-body">
                                <form action="IngredientCrudServlet" method="post">
                                    <input type="hidden" id="recipeId" name="recipe_id" value=""/>
                                    <input type="hidden" id="ingredientId" name="ingredient_id" value=""/>

                                    <div class="form-group">
                                        <label class="control-label" for="recipeName">Recipe</label>
                                        <select data-style="btn btn-success" id="recipeName" class="form-control" name="recipe_name" >
                                            <c:forEach items="${allRecipes}" var="rec" >
                                                <option>${rec.recipe_name} </option>
                                            </c:forEach>
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label" for="ingredientName">Ingredient Name</label>
                                        <input type="text" id="ingredientName" class="form-control" name="ingredient_name" value=""/>
                                    </div>
                                    <div class="form-group dropdown">
                                        <label class="control-label" for="category">Unit of Measure</label>
                                        <select class="form-control" data-style="btn btn-success"  name="uom"  id="uom"  >

                                            <option>kg</option>             
                                            <option>g</option>
                                            <option>l</option>
                                            <option>cup</option>
                                            <option>tsp</option>
                                            <option>tblsp</option>


                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label" for="amount">Amount</label>
                                        <input type="text" id="amount" class="form-control" name="amount" value=""/>
                                    </div>
                                    <div class="modal-footer">
                                        <input type="submit" class="btn btn-primary" value="Save Changes"/>
                                        <a href="#" class="btn btn-default" data-dismiss="modal">Cancel</a>
                                </form>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
            <div class="modal fade" id="recipeedit" aria-hidden="true">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal">×</button>
                            <h3>Editing</h3>
                        </div>
                        <div class="modal-body">
                            <form action="RecipeCrudServlet" method="post" name="recipeForm">


                                <input type="hidden" id="recipeId" class="form-control"  name="recipe_id" value=""/>

                                <div class="form-group">
                                    <label class="control-label" for="recipeName">Recipe Name</label>
                                    <input type="text" id="recipeName" class="form-control"  name="recipe_name" value="" />
                                </div>
                                <div class="form-group">
                                    <label class="control-label" for="recipeDesc">Recipe Description</label>
                                    <input type="text" id="recipeDesc" class="form-control"  name="recipe_desc" value="" />
                                </div>

                                <div class="form-group">
                                    <label class="control-label" for="services">Services</label>
                                    <input type="number" id="services" class="form-control"  name="services" value="" />
                                </div>
                                <div class="form-group dropdown">
                                    <label class="control-label" for="category">Category</label>
                                    <select class="form-control" data-style="btn btn-success"  name="category"  id="category"  >
                                        <c:forEach items="${allCategories}" var="cat">
                                            <option>${cat.category_name} </option>
                                        </c:forEach>

                                    </select>
                                </div>

                                <div class="modal-footer">
                                    <input class="btn btn-primary" type="submit" value="Save Changes"/>
                                    <a href="#" class="btn btn-default" data-dismiss="modal">Cancel</a>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal fade" id="directionEdit" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">×</button>
                        <h3>Editing</h3>
                    </div>
                    <div class="modal-body">
                        <form action="DirectionCrudServlet" method="post">

                            <input type="hidden" id="directionId" name="direction_id" value=""/>
                            <div class="form-group">
                                <label class="control-label" for="step">Step</label>
                                <input type="number" id="step" class="form-control" name="step" value=""/>
                            </div>
                            <div class="form-group">
                                <label class="control-label" for="direction">Direction</label>
                                <input type="text" id="direction" class="form-control" name="direction" value=""/>
                            </div>
                            <div class="form-group dropdown">
                                <label class="control-label" for="recipe">Recipe</label>
                                <select data-style="btn btn-success" id="recipeName" class="form-control" name="recipe_name" value="">
                                    <c:forEach items="${allRecipes}" var="rec" >
                                        <option>${rec.recipe_name} </option>
                                    </c:forEach>
                                </select>
                            </div>
                            <div class="modal-footer">
                                <input class="btn btn-primary" type="submit" value="Save Changes"/>
                                <a href="#" class="btn btn-default" data-dismiss="modal">Cancel</a>
                        </form>
                    </div>

                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="imageEdit" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">×</button>
                    <h3>Editing</h3>
                </div>
                <div class="modal-body">
                    <form action="ImageCrudServlet" method="post">

                        <input type="hidden" id="imageId" name="image_id" value=""/>
                        <div class="form-group">
                            <label class="control-label" for="recipeName">Recipe Name</label>
                            <select data-style="btn btn-success" id="recipeName" class="form-control" name="recipe_name" value="">
                                <c:forEach items="${allRecipes}" var="rec" >
                                    <option>${rec.recipe_name} </option>
                                </c:forEach>
                                    
                            </select>
                        </div>
                        <div class="modal-footer">
                            <input class="btn btn-primary" type="submit" value="Save Changes"/>
                            <a href="#" class="btn btn-default" data-dismiss="modal">Cancel</a>
                    </form>
                </div>

            </div>
        </div>
    </div>
</div>          
<div class="modal fade" id="ingredientAdd" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">×</button>
                <h3>Adding</h3>
            </div>
            <div class="modal-body">
                <form action="NewIngr.do" method="post">
                    <div class="form-group">
                        <label class="control-label" for="recipeName">Recipe</label>
                        <select data-style="btn btn-success" id="recipeName" class="form-control" name="recipe_name" >
                            <c:forEach items="${allRecipes}" var="rec" >
                                <option>${rec.recipe_name} </option>
                            </c:forEach>
                        </select>
                    </div>
                    <div class="form-group">
                        <label class="control-label" for="ingredientName">Ingredient Name</label>
                        <input type="text" id="ingredientName" class="form-control" name="ingredient_name" value=""/>
                    </div>
                    <div class="form-group dropdown">
                        <label class="control-label" for="category">Unit of Measure</label>
                        <select class="form-control" data-style="btn btn-success"  name="uom"  id="uom"  >

                            <option>kg</option>             
                            <option>g</option>
                            <option>l</option>
                            <option>cup</option>
                            <option>tsp</option>
                            <option>tblsp</option>


                        </select>
                    </div>
                    <div class="form-group">
                        <label class="control-label" for="amount">Amount</label>
                        <input type='text' id="amount" class="form-control" name="amount" value=""/>
                    </div>
                    <div class="modal-footer">
                        <input type="submit" class="btn btn-primary" value="Save Changes"/>
                        <a href="#" class="btn btn-default" data-dismiss="modal">Cancel</a>
                </form>
            </div>
        </div>

    </div>
</div>
</div>
<div class="modal fade" id="directionAdd" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">×</button>
                <h3>Adding</h3>
            </div>
            <div class="modal-body">
                <form action="NewDirect.do" method="post">


                    <div class="form-group">
                        <label class="control-label" for="step">Step</label>
                        <input type="number" id="step" class="form-control" name="step" value=""/>
                    </div>
                    <div class="form-group">
                        <label class="control-label" for="direction">Direction</label>
                        <input type="text" id="direction" class="form-control" name="direction" value=""/>
                    </div>
                    <div class="form-group dropdown">
                        <label class="control-label" for="recipe">Recipe</label>
                        <select data-style="btn btn-success" id="recipeName" class="form-control" name="recipe_name" value="">
                            <c:forEach items="${allRecipes}" var="rec" >
                                <option>${rec.recipe_name} </option>
                            </c:forEach>
                        </select>
                    </div>
                    <div class="modal-footer">
                        <input class="btn btn-primary" type="submit" value="Save Changes"/>
                        <a href="#" class="btn btn-default" data-dismiss="modal">Cancel</a>
                </form>
            </div>

        </div>
    </div>
</div>
</div>

<div class="modal fade" id="imageAdd" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">×</button>
                <h3>Adding</h3>
            </div>
            <div class="modal-body">
                <form action="" method="post">

                   
                    <div class="form-group">
                        <label class="control-label" for="recipeName">Recipe Name</label>
                        <select data-style="btn btn-success" id="recipeName5" class="form-control" name="recipe_name" value="">
                            <c:forEach items="${allRecipes}" var="rec" >
                                <option>${rec.recipe_name} </option>
                            </c:forEach>
                        </select>
                    </div>
<!--                       <div class="box-content col-md-6">
                                <div class="form-group">
                                    <div class="table">
                                        <div class="tableRow">
                                            <div class="tableCell">-->
                                                <i class="glyphicon glyphicon-camera"></i>
                                                <input type="file" id="file">
<!--                                            </div>-->
                                            <div  class="tableCell" id="fileInfo" style="visibility: hidden">
                                            </div>
<!--                                        </div>-->
                                        <div id='imagediv' class="tableCell box1" style="visibility: hidden">
                                            <h2>Image</h2>
                                            <img src="" id="uploadedImage" style="width: 170px; height: 100px; background-color:#efeeee; border: 0px; ">
                                            <br>
                                           
                                     </div>
<!--                                </div>
                            </div> -->
                    <div class="modal-footer">
                        <button  id="uploadImage" class="btn btn-primary"> Upload </button>
                        <a href="#" class="btn btn-default" data-dismiss="modal">Cancel</a>
                
            </div>

        </div>
    </div>
</div>
</div>         
<div class="modal fade" id="setting" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">×</button>
                <h3>Settings</h3>
            </div>
            <div class="modal-body">

            </div>
            <div class="modal-footer">
                <a href="#" class="btn btn-primary" data-dismiss="modal">Close</a>
                <a href="#" class="btn btn-primary" data-dismiss="modal">Save changes</a>
            </div>
        </div>
    </div>
</div>

<script src="Admin.js"></script>
<script src="uploadNewImage.js"></script>


</body>
</html>