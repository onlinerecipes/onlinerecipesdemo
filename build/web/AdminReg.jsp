<%-- 
    Document   : AdminReg
    Created on : 04.03.2016, 16:21:33
    Author     : Natavan
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
      <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7" crossorigin="anonymous">
          <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
         <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js" integrity="sha384-0mSbJDEHialfmuBBQP6A4Qrprq5OVfW37PRR3j5ELqxss1yVqOtnepnHVP9aJ7xS" crossorigin="anonymous"></script>
       
         <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7" crossorigin="anonymous">
         <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js" integrity="sha384-0mSbJDEHialfmuBBQP6A4Qrprq5OVfW37PRR3j5ELqxss1yVqOtnepnHVP9aJ7xS" crossorigin="anonymous"></script>
        <title>Authorization</title>
    </head>
    <style>
        #logo{
            margin-top: -15px;
        } 
        body{
            padding:100px;
        }
        input {
        margin-right: 10%;
        width:200px;
        }
        label{
            width:200px;
        }
        button{
            margin: 20px;
        }
        .navbar-fixed-bottom{
            height:10px;
        }
    </style>
    <body>
        <div class = "navbar navbar-default navbar-fixed-top" role = "navigation">
            <div class = "container">
                <div class = "navbar-header">                   
                    <a class = "navbar-brand"><img id = "logo" src = "img/lastversionlogo copy.png" width = 100 height = 90/></a>
                    <a class = "navbar-brand" href="#">Granny's Kitchen</a>
                </div>
            </div>
        </div>
        
        <div class="container">
            <div class="page-header">
                <h1>Authorization</h1>
            </div>
            <div class="jumbotron text-center">
                <form class="form-horizontal" action="Authorization" method="post">
                    <div class="control-group">
                        
                      <label class="control-label" for="inputLogin"><i class="glyphicon glyphicon-user"></i></label>                  
                        <input type="text" name="login" id="inputLogin" placeholder="Login">                      
                        </div>
                        <div class="control-group">
                      <label class="control-label" for="inputPassword"><i class="glyphicon glyphicon-lock green"></i></label>                         
                        <input type="password" name="pass" id="inputPassword" placeholder="Password">                         
                        </div>                                                   
                       <button type="submit" class="btn btn-success">Sign in</button>                          
                 </form>
            </div>
        </div>
         <div class="navbar navbar-inverse navbar-fixed-bottom" role="navigation">
            <div class="container">
                <div class="navbar-text pull-left">
                   <p>© 2016 Granny's Tandir</p>
                </div>
            </div>
            </div>
    </body>
</html>
