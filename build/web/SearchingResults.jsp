<%-- 
    Document   : SearchingResults
    Created on : Apr 17, 2016, 2:03:49 PM
    Author     : User
--%>
<%@page errorPage="ErrorPage.jsp"%>
<%@page import="com.recipes.entities.Ingredient"%>
<%@page import="java.util.Iterator"%>
<%@page import="java.util.List"%>
<%@page import="com.recipes.entities.Recipe"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7" crossorigin="anonymous">
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js" integrity="sha384-0mSbJDEHialfmuBBQP6A4Qrprq5OVfW37PRR3j5ELqxss1yVqOtnepnHVP9aJ7xS" crossorigin="anonymous"></script>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
        <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7" crossorigin="anonymous">
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js" integrity="sha384-0mSbJDEHialfmuBBQP6A4Qrprq5OVfW37PRR3j5ELqxss1yVqOtnepnHVP9aJ7xS" crossorigin="anonymous"></script>
        <title>Granny's Tandir</title>
    </head>
    <style>
        #recipeName{
            border: none;
            background: none;
        }
        </style>
    <body>
         <c:import url="/WEB-INF/tags/header.tag"/>
         <%
             if(request.getAttribute("result") != null){
         %>
         <table class="table table-striped" id="disease_table">
    <thead>
    <tr>
         <th>Recipe Name</th>
         <th>Short Description</th>
         <th>Ingredient</th>
    </tr>
    <tr>
        <%
            List l = (List) request.getAttribute("result");
            Iterator iter = l.iterator();
            Recipe previous = null;
            while(iter.hasNext()){         
            Ingredient pre = (Ingredient) iter.next();            
            request.setAttribute("pre",pre);
            if(pre.getRecipe() != previous){
                previous = pre.getRecipe();
        %>
        
    <form action="ShowRecipe">        
        <input type="hidden" name="recipenm" value="${pre.getRecipe().getRecipe_id()}"/>
         <th><input id="recipeName" type="Submit" value="${pre.getRecipe().getRecipe_name()}"/></th>
    </form>
    
    <th>${pre.getRecipe().getRecipe_desc()}</th>
    </tr>
    </thead>
        <%
            }//if stmnt
        %>
    <tbody>
                   
                        <tr>
                            <td></td>        <!-- space for name -->                    
                            <td></td>       <!-- space for description --> 
                            <td>${pre.ingredient_name}</td> <!-- ingredients --> 
                       </tr>
                       <%
            }//while loop
                       %>
    </tbody>
    </table>
         <%}
         
             else{%>
    <center><br /><h3>No matching result has found<h3></center>
             <%}%>
             
          
    </body>
</html>
