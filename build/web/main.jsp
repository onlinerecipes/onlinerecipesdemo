<%--<%@page errorPage="ErrorPage.jsp"%>--%>
<%@page import="com.recipes.Db.CategoryDb"%>
<%@page import="com.recipes.entities.Image"%>
<%@page import="com.recipes.entities.Direction"%>
<%@page import="com.recipes.entities.Ingredient"%>
<%@page import="com.recipes.entities.Recipe"%>
<%@page import="java.util.Iterator"%>
<%@page import="java.util.List"%>
<%@page import="com.recipes.Db.RecipeDb"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="en">

<head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">	
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Granny’s Tandir</title>
	<meta name="description" content=“Granny’s Tandir has been made for Terms Project” />
	<meta name="keywords" content="html template, css, free, one page, gym, fitness, web design" />
	<meta name="author" content=“Nargis A.” />
	<!-- Favicons (created with http://realfavicongenerator.net/)-->
	<link rel="apple-touch-icon" sizes="57x57" href="img/favicons/apple-touch-icon-57x57.png">
	<link rel="apple-touch-icon" sizes="60x60" href="img/favicons/apple-touch-icon-60x60.png">
	<link rel="icon" type="image/png" href="img/favicons/favicon-32x32.png" sizes="32x32">
	<link rel="icon" type="image/png" href="img/favicons/favicon-16x16.png" sizes="16x16">
	<link rel="manifest" href="img/favicons/manifest.json">
	<link rel="shortcut icon" href="img/favicons/favicon.ico">
	<meta name="msapplication-TileColor" content="#00a8ff">
	<meta name="msapplication-config" content="img/favicons/browserconfig.xml">
	<meta name="theme-color" content="#ffffff">
	<!-- Normalize -->
	<link rel="stylesheet" type="text/css" href="css/normalize.css">
	<!-- Bootstrap -->
	<link rel="stylesheet" type="text/css" href="css/bootstrap.css">
	<!-- Owl -->
	<link rel="stylesheet" type="text/css" href="css/owl.css">
	<!-- Animate.css -->
	<link rel="stylesheet" type="text/css" href="css/animate.css">
	<!-- Font Awesome -->
	<link rel="stylesheet" type="text/css" href="fonts/font-awesome-4.1.0/css/font-awesome.min.css">
	<!-- Elegant Icons -->
	<link rel="stylesheet" type="text/css" href="fonts/eleganticons/et-icons.css">
	<!-- Main style -->
	<link rel="stylesheet" type="text/css" href="css/grannystandir.css">
<link rel="stylesheet" type="text/css" href="css/2ndsearch.css">
</head>
 <link href='custom.css' rel='stylesheet' type='text/css'>
<link rel="stylesheet" type="text/css" href="css/seacrh.css">

    </head>

<body>
    <% request.setAttribute("AllCategories", CategoryDb.getAllCategories()); %>
	<div class="preloader">
		<img src="img/loader.gif" alt="Preloader image">
	</div>
	<nav class="navbar">
		<div class="container">
			<!-- Brand and toggle get grouped for better mobile display -->
			<div class="navbar-header">
				<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
					<span class="sr-only">Toggle navigation</span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
				</button>
				<a class="navbar-brand" href="main.jsp"><img src="img/lastversionlogo copy.png" data-active-url="img/lastversionlogo copy.png" alt=""></a>
			</div>
			<!-- Collect the nav links, forms, and other content for toggling -->
			<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
				<ul class="nav navbar-nav navbar-right main-nav">
					<li><a href="#home">Home</a></li>
					<li><a href="#about">About</a></li>
					<li><a href="#recipes">Recipes</a></li>
					<li><a href="#contact">Contact</a></li>
					
				</ul>
			</div>
			<!-- /.navbar-collapse -->
		</div>
		<!-- /.container-fluid -->
	</nav>
	<header id="home">
		<div class="home">
			<div class="table">
				<div class="header-text">
					<div class="row">
						<div class="col-md-12 text-center">
							<h3 class="light white">Learn how to cook with Granny.</h3>
<br><br/>
							<h1 class="white typed">Only place you can learn how to cook Azerbaijani meals in Granny’s way.</h1>
							<span class="typed-cursor">|</span>
						</div>
					</div>
				</div>
			</div>
		</div>
	</header>
	<section>
             <%     
            List l = RecipeDb.getFirstNRecipes(5);
//            Iterator iter = l.iterator();
//            while(iter.hasNext()){    
            if(l!=null&&l.size()==5){
            Recipe r1 = (Recipe) l.get(0);
            request.setAttribute("r1",r1); 
             Recipe r2 = (Recipe) l.get(1);
            request.setAttribute("r2",r2); 
            Recipe r3 = (Recipe) l.get(2);
            request.setAttribute("r3",r3);
             Recipe r4 = (Recipe) l.get(3);
             request.setAttribute("r4",r4); 
             Recipe r5 = (Recipe) l.get(4);
             request.setAttribute("r5",r5); 
            
            }
             %>
		<div class="cut cut-top"></div>
		<div class="container">
			<div class="row home-tables">
				<div class="col-md-4">
					<div class="home-table home-table-first">
						<h5 class="white heading">Top Rated Recipes</h5>
						<div class="owl-carousel owl-schedule bottom">
							<div class="item">
								<div class="schedule-row row">
									<div class="col-xs-6">
                                                                           <form action="ShowRecipe">
                                                                            <input type="hidden" name="recipenm" value="${r1.getRecipe_id()}"/>
										<h5 class="regular white"><input style="background-color: transparent; border: none" id="recipeName" type="Submit" value="${r1.getRecipe_name()}"/></h5>
                                                                           </form>
                                                                        </div>
									<div class="col-xs-6 text-right">
										<h5 class="white"> ${r1.getRating()}</h5>
									</div>
								</div>
								<div class="schedule-row row">
									<div class="col-xs-6">
										<h5 class="regular white">${r1.getRecipe_desc()}</h5>
									</div>
									<div class="col-xs-6 text-right">
										<h5 class="white">Description</h5>
									</div>
								</div>
								<div class="schedule-row row">
									<div class="col-xs-6">
										<h5 class="regular white">${r1.getCategory().getCategory_name()}</h5>
									</div>
									<div class="col-xs-6 text-right">
										<h5 class="white">Category</h5>
									</div>
								</div>
							</div>
							<div class="item">
								<div class="schedule-row row">
									<div class="col-xs-6">
                                                                            <form action="ShowRecipe">
                                                                            <input type="hidden" name="recipenm" value="${r2.getRecipe_id()}"/>
										<h5 class="regular white"><input style="background-color: transparent; border: none;" id="recipeName" type="Submit" value="${r2.getRecipe_name()}"/></h5>
                                                                            </form>
                                                                        </div>
									<div class="col-xs-6 text-right">
										<h5 class="white"> ${r2.getRating()}</h5>
									</div>
								</div>
								<div class="schedule-row row">
									<div class="col-xs-6">
										<h5 class="regular white">${r2.getRecipe_desc()}</h5>
									</div>
									<div class="col-xs-6 text-right">
										<h5 class="white">Description</h5>
									</div>
								</div>
								<div class="schedule-row row">
									<div class="col-xs-6">
										<h5 class="regular white">${r2.getCategory().getCategory_name()}</h5>
									</div>
									<div class="col-xs-6 text-right">
										<h5 class="white">Category</h5>
									</div>
								</div>
							</div>
							<div class="item">
								<div class="schedule-row row">
									<div class="col-xs-6">
                                                                            <form action="ShowRecipe">
                                                                            <input type="hidden" name="recipenm" value="${r3.getRecipe_id()}"/>
										<h5 class="regular white"><input style="background-color: transparent; border: none;" id="recipeName" type="Submit" value="${r3.getRecipe_name()}"/></h5>
                                                                            </form>
                                                                        </div>
                                                                        
									<div class="col-xs-6 text-right">
										<h5 class="white"> ${r3.getRating()}</h5>
									</div>
								</div>
								<div class="schedule-row row">
									<div class="col-xs-6">
										<h5 class="regular white">${r3.getRecipe_desc()}</h5>
									</div>
									<div class="col-xs-6 text-right">
										<h5 class="white">Description</h5>
									</div>
								</div>
								<div class="schedule-row row">
									<div class="col-xs-6">
										<h5 class="regular white">${r3.getCategory().getCategory_name()}</h5>
									</div>
									<div class="col-xs-6 text-right">
										<h5 class="white">Category</h5>
									</div>
								</div>
							</div>
                </div>
                                        </div>
                                </div>
				<div class="col-md-4">
					<div class="home-table home-table-hover">
					
					</div>
				</div>
                            
				<div class="col-md-4">
					<div class="home-table home-table-third">
						<h5 class="white heading">Top Rated Recipes</h5>
						<div class="owl-carousel owl-schedule bottom">
							<div class="item">
								<div class="schedule-row row">
									<div class="col-xs-6">
                                                                            <form action="ShowRecipe">
                                                                            <input type="hidden" name="recipenm" value="${r4.getRecipe_id()}"/>
										<h5 class="regular white"><input style="background-color: transparent; border: none;" id="recipeName" type="Submit" value="${r4.getRecipe_name()}"/></h5>
									</form>
                                                                        </div>
									<div class="col-xs-6 text-right">
										<h5 class="white"> ${r4.getRating()}</h5>
									</div>
								</div>
								<div class="schedule-row row">
									<div class="col-xs-6">
										<h5 class="regular white">${r4.getRecipe_desc()}</h5>
									</div>
									<div class="col-xs-6 text-right">
										<h5 class="white">Description</h5>
									</div>
								</div>
								<div class="schedule-row row">
									<div class="col-xs-6">
										<h5 class="regular white">${r4.getCategory().getCategory_name()}</h5>
									</div>
									<div class="col-xs-6 text-right">
										<h5 class="white">Category</h5>
									</div>
								</div>
							</div>
							<div class="item">
								<div class="schedule-row row">
									<div class="col-xs-6">
                                                                            <form action="ShowRecipe">
                                                                            <input type="hidden" name="recipenm" value="${r5.getRecipe_id()}"/>
										<h5 class="regular white"><input style="background-color: transparent; border: none;" id="recipeName" type="Submit" value="${r5.getRecipe_name()}"/></h5>
                                                                            </form>
                                                                        </div>
									<div class="col-xs-6 text-right">
										<h5 class="white"> ${r5.getRating()}</h5>
									</div>
								</div>
								<div class="schedule-row row">
									<div class="col-xs-6">
										<h5 class="regular white">${r5.getRecipe_desc()}</h5>
									</div>
									<div class="col-xs-6 text-right">
										<h5 class="white">Description</h5>
									</div>
								</div>
								<div class="schedule-row row">
									<div class="col-xs-6">
										<h5 class="regular white">${r5.getCategory().getCategory_name()}</h5>
									</div>
									<div class="col-xs-6 text-right">
										<h5 class="white">Category</h5>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
                        </div>
                </div>
	</section>
	<section id="about" class="section section-padded">
		<div class="container">
			<div class="row text-center title">
				<h2>About</h2>
				<h4 class="light muted"> “Granny’s Tandir” is a web application that every person from any part of the world can use in order to get information about Azerbaijani cuisine and how to cook those great meals. The admin of the site is  an Azerbaijani woman whose wish was to share all her precious recipes with everyone. </h4>
			</div>
			<div class="row about">
				<div class="col-md-4">
					<div class="about1">
						<div class="icon-holder">
							<img src="img/icons/1st.PNG" alt="" class="icon">
						</div>
						<h4 class="heading">Who is Granny?</h4>
						<p class="description">Banuchichek, an Azerbaijani woman, 68 years old, who studied IT. She has been collecting all recipes since she was 14 years old.</p>
					</div>
				</div>
				<div class="col-md-4">
					<div class="about1">
						<div class="icon-holder">
							<img src="img/icons/2nd.png" alt="" class="icon">
						</div>
						<h4 class="heading">Why “Grannys Tandir”?</h4>
						<p class="description">Tandir-the term refers to a cylindrical clay used in cooking and baking.It’s used for cooking in the Caucasus.Mostly, Azerbaijani meals can be cooked in Tandir in order to get very specific flavor.</p>
					</div>
				</div>
				<div class="col-md-4">
					<div class="about1">
						<div class="icon-holder">
							<img src="img/icons/3rd.png"  alt="" class="icon">
						</div>
						<h4 class="heading">What makes this site special?</h4>
						<p class="description">Granny has changed every single recipes in different way, so that you will be able to feel the difference.</p>
					</div>
				</div>
			</div>
		</div>
		<div class="cut cut-bottom"></div>
	</section>
	<section id="recipes" class="section gray-bg">
		<div class="container">
			<div class="row title text-center">
				<h2 class="margin-top">Recipes</h2>
				<h4 class="light muted">Search and find what you are going to make!</h4>
<h5 class="light muted">And here are Granny’s 3 favorite recipes:</h5>
			</div>
                </div>
<div class="box">
  <div class="container-4">
      <form role="search" action="SearchController" method="post">
          <input type="search" id="search" name="search" placeholder="Search..."/>
    <button class="icon"><i class="fa fa-search"></i></button>
      </form>
  </div>
</div> 

			<div class="row">
				<div class="col-md-4">
					<div class="recipes text-center">
						<div class="cover" style="background:url('img/team/Dill-pilaf.PNG'); background-size:cover;">
							<div class="overlay text-center">
								<h3 class="white"></h3>
								<h5 class="light light-white"><b>Azerbaijani cuisine has a rich variety of pilaf dishes.<b></h5>
							</div>
						</div>
						
						<div class="title">
							<h4>Dill pilaf recipe (Shuyud plov) </h4>
							<h5 class="muted regular"></h5>
						</div>
						
					</div>
				</div>
				<div class="col-md-4">
					<div class="recipes text-center">
						<div class="cover" style="background:url('img/team/Savoury-pastry.PNG'); background-size:cover;">
							<div class="overlay text-center">
								<h3 class="white"></h3>
								<h5 class="light light-white"><b>Shor gogal is a popular savoury pastry, made from layers of pastry flavoured with turmeric and fennel seeds.</b></h5>
							</div>
						</div>
						
						<div class="title">
							<h4>Savoury pastry recipe (Shor gogal)</h4>
							<h5 class="muted regular"></h5>
						</div>

					</div>
				</div>
<div class="col-md-4">
					<div class="recipes text-center">
						<div class="cover" style="background:url('img/team/minced-lamb.PNG'); background-size:cover;">
							<div class="overlay text-center">
								<h3 class="white"></h3>
								<h5 class="light light-white"><b> Kebabs – barbecued meat, fish and vegetables – are a perennial favourite in Azerbaijan.</b> </h5>
							</div>
						</div>
						
						<div class="title">
							<h4>Minced lamb recipe (Lula kabab)</h4>
							<h5 class="muted regular"></h5>



				
	</section>
	<section id="contact" class="section">
		<div class="container">
			<div class="row title text-center">
				<h2 class="margin-top">Contact</h2>
				<h4>Would you like to share some receipts with me? I would love to see your amazing receipts. Maybe we can synthesis some of them?</h4>


                        </div>
                </div>
        <div class="container">

            <div class="row">

                <div class="col-lg-8 col-lg-offset-2">

                   

                    <form id="contact-form" method="post" action="message.send.do" role="form">

                        <div class="messages"></div>

                        <div class="controls">

                            <div class="row">
                                <div class="col-md-6">
                                    <label for="form_name">Name *</label>
                                    <input id="form_name" type="text" name="userName" class="form-control" placeholder="Please enter your name *" required="required">
                                </div>
                               
                                <div class="col-md-6">
                                    <label for="form_email">Email *</label>
                                    <input id="form_email" type="email" name="userEmail" class="form-control" placeholder="Please enter your email *" required="required">
                                </div>
                              
                                <div class="col-md-12">
                                    <label for="form_message">Message *</label>
                                    <textarea id="form_message" name="userMessage" class="form-control" placeholder="Message for me *" rows="4" required="required"></textarea>
                                </div>
                                <div class="col-md-12">
                                    <input type="submit" class="btn btn-success btn-send" value="Send message">
                                </div>
                         
                                <div class="col-md-12">
                                    <p class="text-muted"><strong>*</strong> These fields are required.</p>
                                </div>
                                    
                            </div>
                        </div>

                    </form>

                </div><!-- /.8 -->

            </div> <!-- /.row-->

        </div> <!-- /.container-->

        <script src="https://code.jquery.com/jquery-1.12.0.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
        <script src="contact.js"></script>
    </body>	
					
	</section>
	
	<footer>
		
			<div class="row">
				<div class="col-sm-6 text-center-mobile">
					<h3 class="white">Practice more and more, dear!</h3>
					<h5 class="light regular light-white">You will surely be good at cooking.</h5>
					<a href="main.jsp" class="btn btn-blue ripple trial-button">Return to Home Page</a>
				</div>
				<div class="col-sm-6 text-center-mobile">
					<h3 class="white">Green Light is always here for you! <span class="open-blink"></span></h3>
					<div class="row opening-hours">
						<div class="col-sm-6 text-center-mobile">
							<h5 class="light-white light">Darling, keep in mind</h5>
							<h3 class="regular white">just try it.</h3>
						</div>
						<div class="col-sm-6 text-center-mobile">
							<h5 class="light-white light">Granny is always here to help you</h5>
							<h3 class="regular white">How about one more recipe?</h3>
						</div>
					</div>
				</div>
			</div>
			<div class="row bottom-footer text-center-f">
				<div class="col-sm-8">
					<p>&copy; 2016 All Rights Reserved.</p>
				</div>
				<div class="col-sm-4 text-right text-center-mobile">
					
				</div>
			</div>
		
	</footer>
	<!-- Holder for mobile navigation -->
	<div class="mobile-nav">
		<ul>
		</ul>
		<a href="#" class="close-link"><i class="arrow_up"></i></a>
	</div>
	<!-- Scripts -->
	<script src="js/jquery-1.11.1.min.js"></script>
	<script src="js/owl.carousel.min.js"></script>
	<script src="js/bootstrap.min.js"></script>
	<script src="js/wow.min.js"></script>
	<script src="js/typewriter.js"></script>
	<script src="js/jquery.onepagenav.js"></script>
	<script src="js/main.js"></script>
</body>

</html>
