

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 * author: Natavan
 */
$(document).ready(function () {
    $('.btn-close').click(function (e) {
        e.preventDefault();
        $(this).parent().parent().parent().fadeOut();
    });
    $('.btn-minimize').click(function (e) {
        e.preventDefault();
        s
        var $target = $(this).parent().parent().next('.box-content');
        if ($target.is(':visible'))
            $('i', $(this)).removeClass('glyphicon-chevron-up').addClass('glyphicon-chevron-down');
        else
            $('i', $(this)).removeClass('glyphicon-chevron-down').addClass('glyphicon-chevron-up');
        $target.slideToggle();
    });
    $('.btn-setting').click(function (e) {
        e.preventDefault();
        $('#setting').modal('show');
    });
    $('.recipe-edit').click(function (e) {
        e.preventDefault();
        $('.modal-body #recipeDesc').val($(this).data('id2'));
        $('.modal-body #recipeName').val($(this).data('id'));
        $('.modal-body #recipeId').val($(this).data('id0'));

        $('.modal-body #services').val($(this).data('id4'));
        $('.modal-body #category').val($(this).data('id5'));

        $('#recipeedit').modal('show');


    });
    $('.ingredient-edit').click(function (e) {
        e.preventDefault();
        $('.modal-body #ingredientId').val($(this).data('ingredientid'));
        $('.modal-body #ingredientName').val($(this).data('ingredientname'));
        $('.modal-body #recipeName').val($(this).data('recipename'));
        $('.modal-body #recipeId').val($(this).data('recipeid'));
        $('.modal-body #uom').val($(this).data('uom'));
        $('.modal-body #amount').val($(this).data('amount'));

        $('#ingredientEdit').modal('show');
    });
    $('.ingredient-add').click(function (e) {
        e.preventDefault();
        $('#ingredientAdd').modal('show');
    });

    $('.direction-edit').click(function (e) {
        e.preventDefault();
        $('.modal-body #direction').val($(this).data('direction'));
        $('.modal-body #step').val($(this).data('step'));
        $('.modal-body #directionId').val($(this).data('directionid'));
        $('.modal-body #recipeName').val($(this).data('recipename'));
        $('#directionEdit').modal('show');


    });
    $('.direction-add').click(function (e) {
        e.preventDefault();

        $('#directionAdd').modal('show');


    });
    $('.image-edit').click(function (e) {
        e.preventDefault();
        $('.modal-body #imageId').val($(this).data('imageid'));
        $('.modal-body #recipeName').val($(this).data('recipename'));
        $('#imageEdit').modal('show');


    });
    $('.image-add').click(function (e) {
        e.preventDefault();
        $('#imageAdd').modal('show');


    });
    $('.category-add').click(function (e) {
        e.preventDefault();
        $('#addCategory').modal('show');
    });
   
});
