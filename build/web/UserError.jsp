<%-- 
    Document   : ErrorPage
    Created on : Mar 30, 2016, 11:47:58 PM
    Author     : Sariya
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7" crossorigin="anonymous">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js" integrity="sha384-0mSbJDEHialfmuBBQP6A4Qrprq5OVfW37PRR3j5ELqxss1yVqOtnepnHVP9aJ7xS" crossorigin="anonymous"></script>
        <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7" crossorigin="anonymous">
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js" integrity="sha384-0mSbJDEHialfmuBBQP6A4Qrprq5OVfW37PRR3j5ELqxss1yVqOtnepnHVP9aJ7xS" crossorigin="anonymous"></script>
        <title>Admin Panel</title>
    </head>

    <body>

        <style>
            a:hover{
                text-decoration: none;
            }            
            ul{
                list-style: none;
            }
            ul li{
                float: left;
                margin:10px 10px 0px;
            }
            .dropdown-header{
                width:100%;
            }
            .table {
                display: table;
                border-collapse: separate;
                border-spacing: 40px 50px;
            }
            .tableRow {
                display: table-row;
            }
            .tableCell {
                display: table-cell;
            }
            .box1 {
                border: 10px solid rgba(238, 238, 238, 0.5);
                width: 250px;
                height: 200px;
            }            
        </style>
         <c:import url="/WEB-INF/tags/header.tag"/>
</br>
</br>
</br>
</br>

        <div class="ch-container row">
           <center> 
            <div id="errorfield" class="col-8-xm" >
                  <div class="alert alert-danger" role="alert" >
                 <h4>  We love our users. But perhaps you should stop spamming.
                   <a href="main.jsp" class="alert-link">Go back to main page</a> </h4>
                 </div>
            </div>                     
           </center>
        </div>
        <script src="uploadfile.js"></script>
    </body>
</html>
