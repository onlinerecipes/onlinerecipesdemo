<%-- 
    Document   : SearchingResults
    Created on : Apr 17, 2016, 2:03:49 PM
    Author     : User
--%>
<%@page errorPage="ErrorPage.jsp"%>
<%@page import="com.recipes.Db.CategoryDb"%>
<%@page import="com.recipes.Db.RecipeDb"%>
<%@page import="com.recipes.entities.Ingredient"%>
<%@page import="java.util.Iterator"%>
<%@page import="java.util.List"%>
<%@page import="com.recipes.entities.Recipe"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7" crossorigin="anonymous">
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js" integrity="sha384-0mSbJDEHialfmuBBQP6A4Qrprq5OVfW37PRR3j5ELqxss1yVqOtnepnHVP9aJ7xS" crossorigin="anonymous"></script>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
        <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7" crossorigin="anonymous">
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js" integrity="sha384-0mSbJDEHialfmuBBQP6A4Qrprq5OVfW37PRR3j5ELqxss1yVqOtnepnHVP9aJ7xS" crossorigin="anonymous"></script>
        <title>Granny's Tandir</title>
    </head>
    <style>
        #recipeName{
            border: none;
            background: none;
        }
    </style>
    <body>
        <c:import url="/WEB-INF/tags/header.tag"/>
        <%
            String i = request.getParameter("id");
            int id = 0;
            if (!i.isEmpty()) {
                id = Integer.parseInt(i);
            }
            System.out.println(i);
            System.out.println(RecipeDb.getRecipeByCategoryId(CategoryDb.getCategoryById(id)));
            List<Recipe> recipes=RecipeDb.getRecipeByCategoryId(CategoryDb.getCategoryById(id));
            request.setAttribute("recipes", recipes);
        %>
        </br>
        </br>
        </br>
        <div class="container">
            <div class="row">
                <div class="col-sm-8"> 
                    
                            <c:forEach items="${recipes}" var="rec">
                            <a href="ShowRecipe?recipenm=${rec.getImage().getRecipe().recipe_id}">
                                 <h4> ${rec.getImage().getRecipe().recipe_name} </h4>
                                 
                              
                                <img  src="uploaded-images/${rec.getImage().image_name}" style="border-radius: 100px; width: 150px; height: 150px">
                            </a>
                            </c:forEach>
                        
                     
                    
                </div>
            </div>
        </div>
    </body>
</html>
