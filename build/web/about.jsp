<%-- 
    Document   : about
    Created on : 08.03.2016, 21:35:27
    Author     : Natavan
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
         <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7" crossorigin="anonymous">
         <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js" integrity="sha384-0mSbJDEHialfmuBBQP6A4Qrprq5OVfW37PRR3j5ELqxss1yVqOtnepnHVP9aJ7xS" crossorigin="anonymous"></script>
         <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
         <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7" crossorigin="anonymous">
         <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js" integrity="sha384-0mSbJDEHialfmuBBQP6A4Qrprq5OVfW37PRR3j5ELqxss1yVqOtnepnHVP9aJ7xS" crossorigin="anonymous"></script>
        <title>About us</title>
    </head>
    <style>
        #logo{
            margin-top: -15px;
        } 
        jumbotrom{
            padding-top:70px;
            padding-bottom:100px;
        }
        .btn-group{
            width:100px;
        }
        .navbar-inverse{
            height:10px;
        }
        .navbar-inverse{           
            margin-bottom:0;
            margin-top:10%;
        }
    </style>
    
    <body>
        <div class = "navbar navbar-default navbar-top" role = "navigation">
            <div class = "container">
                <div class = "navbar-header">
                    <button type = "button" class="navbar-toggle" data-toggle = "collapse" data-target=".navbar-collapse">
                        <span class = "sr-only">Toggle Navigation</span>
                        <span class = "icon-bar"></span>
                        <span class = "icon-bar"></span>
                        <span class = "icon-bar"></span>
                    </button>
                    <a class = "navbar-brand"><img id = "logo" src = "granny.jpg" width = 50 height = 50/></a>
                    <a class = "navbar-brand" href="main.jsp">Granny's Kithcen</a>
                </div>
                <div class="navbar-collapse collapse">
                    <ul class="nav navbar-nav navbar-right">                  
                        <li><a href="main.jsp"><span class="glyphicon glyphicon-home" aria-hidden="true"></span> Home</a></li>
                        <li class="active"><a href="about.jsp"><span class="glyphicon glyphicon-info-sign" aria-hidden="true"></span> About</a></li>
                        <li><a href="#contact" data-toggle="modal"><span class="glyphicon glyphicon-envelope" aria-hidden="true"></span> Contact</a></li>
                    </ul>
                </div>
            </div>
        </div>
        
        <div class="container">
            <div class="jumbotron text-center">
                <h1 style="color:green">Granny's Kithcen</h1>
                <p>This is a useful application for looking up different recipes.</p>
                <p>Here you can find many types of meal.</p>
                <p>Cooking with us is Tastier!</p>
                </div>
               </form>
            </div>                               
         
        <div class="navbar navbar-inverse navbar-bottom" role="navigation">
            <div class="container">
                <div class="navbar-text pull-left">
                   <p>© 2016 Granny's Kitchen</p>
                </div>
            </div>
            </div>
        
        <!-- Contact-->
          <div class="modal fade" id="contact" roles="dialog">
            <div class="modal-dialog">
                <div class="modal-content">
                    <form class="form-horizontal" role="form">
                        <div class="modal-header">
                            <h4>Contact</h4>
                        </div>
                        <div class="modal-body">
                            <div class="form-group">
                                <label for="contact-name" class="col-sm-2 control-label">Name</label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control" id="contact-name" placeholder="First & Last Name">
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="contact-email" class="col-sm-2 control-label">Email</label>
                                <div class="col-sm-10">
                                    <input type="email" class="form-control" id="contact-email" placeholder="example@domain.com">
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="contact-message" class="col-sm-2 control-label">Message</label>
                                <div class="col-sm-10">
                                    <textarea class="form-control" rows="4"></textarea>
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">                            
                            <a class="btn btn-default" data-dismiss="modal">Close</a>
                            <button type="submit" class="btn btn-primary">Send</button>
                        </div>
                    </form>
                </div>
            </div> 
        </div>
    </body>
</html>
