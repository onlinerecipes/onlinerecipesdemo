package com.recipes.entities;

import com.recipes.entities.Recipe;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2016-04-30T01:41:22")
@StaticMetamodel(Ingredient.class)
public class Ingredient_ { 

    public static volatile SingularAttribute<Ingredient, String> uom;
    public static volatile SingularAttribute<Ingredient, Double> amount;
    public static volatile SingularAttribute<Ingredient, Integer> ingredient_id;
    public static volatile SingularAttribute<Ingredient, String> ingredient_name;
    public static volatile SingularAttribute<Ingredient, Recipe> recipe;

}