package com.recipes.entities;

import com.recipes.entities.Recipe;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2016-04-30T01:41:22")
@StaticMetamodel(Image.class)
public class Image_ { 

    public static volatile SingularAttribute<Image, String> image_name;
    public static volatile SingularAttribute<Image, Recipe> recipe;
    public static volatile SingularAttribute<Image, Integer> image_id;

}