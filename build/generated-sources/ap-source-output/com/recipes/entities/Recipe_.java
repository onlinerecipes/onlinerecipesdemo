package com.recipes.entities;

import com.recipes.entities.Category;
import com.recipes.entities.Comments;
import com.recipes.entities.Direction;
import com.recipes.entities.Image;
import com.recipes.entities.Ingredient;
import javax.annotation.Generated;
import javax.persistence.metamodel.ListAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2016-04-30T01:41:22")
@StaticMetamodel(Recipe.class)
public class Recipe_ { 

    public static volatile SingularAttribute<Recipe, String> recipe_desc;
    public static volatile ListAttribute<Recipe, Image> images;
    public static volatile SingularAttribute<Recipe, Integer> recipe_id;
    public static volatile ListAttribute<Recipe, Comments> comments;
    public static volatile ListAttribute<Recipe, Direction> directions;
    public static volatile SingularAttribute<Recipe, Integer> rating;
    public static volatile SingularAttribute<Recipe, String> recipe_name;
    public static volatile ListAttribute<Recipe, Ingredient> ingredients;
    public static volatile SingularAttribute<Recipe, Integer> services;
    public static volatile SingularAttribute<Recipe, Category> category;

}